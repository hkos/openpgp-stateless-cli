---
title: Stateless OpenPGP Command Line Interface
docname: draft-dkg-openpgp-stateless-cli-12
category: info

ipr: trust200902
area: int
workgroup: openpgp
keyword: Internet-Draft
submissionType: IETF

stand_alone: yes
pi: [toc, sortrefs, symrefs]

author:
 -
    ins: D. K. Gillmor
    name: Daniel Kahn Gillmor
    org: American Civil Liberties Union
    street: 125 Broad St.
    city: New York, NY
    code: 10004
    country: USA
    abbrev: ACLU
    email: dkg@fifthhorseman.net
venue:
  group: "OpenPGP"
  type: "Working Group"
  mail: "openpgp@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/openpgp/"
  repo: "https://gitlab.com/dkg/openpgp-stateless-cli/"
  latest: "https://dkg.gitlab.io/openpgp-stateless-cli/"
informative:
 OpenPGP-Interoperability-Test-Suite:
    target: https://tests.sequoia-pgp.org/
    title: OpenPGP Interoperability Test Suite
    date: 2021-10-25
 Charset-Switching:
  target: https://dkg.fifthhorseman.net/notes/inline-pgp-harmful/
  title: Inline PGP Considered Harmful
  author:
   -
    ins: D. K. Gillmor
    name: Daniel Kahn Gillmor
  date: 2014-02-24
 DISPLAYING-SIGNATURES:
  target: https://admin.hostpoint.ch/pipermail/enigmail-users_enigmail.net/2017-November/004683.html
  title: "On Displaying Signatures"
  author:
   -
    ins: P. Brunschwig
    name: Patrick Brunschwig
 EFAIL:
  target: https://efail.de
  title: "Efail: Breaking S/MIME and OpenPGP Email Encryption using Exfiltration Channels"
  author:
   -
    ins: D. Poddebniak
    name: Damian Poddebniak
   -
    ins: C. Dresen
    name: Christian Dresen
 PYTHON-SOP:
  target: https://pypi.org/project/sop/
  title: SOP for python
  author:
   name: Daniel Kahn Gillmor
   ins: D. Gillmor
 RUST-SOP:
  target: https://sequoia-pgp.gitlab.io/sop-rs/
  title: A Rust implementation of the Stateless OpenPGP Protocol
  author:
   name: Justus Winter
   ins: J. Winter
   org: Sequoia
 SEMVER:
  target: https://semver.org/
  title: Semantic Versioning 2.0.0
  date: 2013-06-18
  author:
   name: Tom Preston-Werner
   ins: T. Preston-Werner
 SOP-JAVA:
  target: https://github.com/pgpainless/sop-java
  title: Stateless OpenPGP Protocol for Java.
  author:
   -
    name: Paul Schaub
    ins: P. Schaub
 UNICODE-NORMALIZATION:
    target: https://unicode.org/reports/tr15/
    title: Unicode Normalization Forms
    date: 2019-02-04
    author:
     name: Ken Whistler
     ins: K. Whistler
     org: Unicode Consortium
 OPENPGP-SMARTCARD:
   target: https://www.gnupg.org/ftp/specs/OpenPGP-smart-card-application-3.4.pdf
   title: Functional Specification of the OpenPGP application on ISO Smart Card Operating Systems, Version 3.4
   date: 2020-03-18
   author:
    -
     name: Achim Pietig
 GNUPG-SECRET-STUB:
   target: "https://dev.gnupg.org/source/gnupg/browse/master/doc/DETAILS;gnupg-2.4.3$1511"
   title: GNU Extensions to the S2K algorithm
   date: 2023-07-04
   author:
    -
     name: Werner Koch
     org: g10 Code
 DKG-SOP:
   title: "dkg-sop"
   author:
    -
      name: Heiko Stamer
   target: "https://git.savannah.nongnu.org/cgit/dkgpg.git/tree/tools/dkg-sop.cc"
 GOSOP:
   title: "gosop"
   author:
    -
     org: Proton
   target: "https://github.com/ProtonMail/gosop"
 GPGME-SOP:
   title: "gpgme-sop"
   author:
    -
     name: Justus Winter
   target: "https://gitlab.com/sequoia-pgp/gpgme-sop"
 PGPAINLESS-CLI:
   title: "pgpainless-cli"
   author:
    -
     name: Paul Schaub
   target: "https://codeberg.org/PGPainless/pgpainless/src/branch/master/pgpainless-sop"
 RNP-SOP:
   title: "rnp-sop"
   author:
    -
     name: Justus Winter
   target: "https://gitlab.com/sequoia-pgp/rnp-sop"
 RSOP:
   title: "rsop"
   author:
    -
     name: Heiko Schaefer
   target: "https://codeberg.org/heiko/rsop"
 SQOP:
   title: "sqop"
   author:
    -
     org: Sequoia PGP
   target: "https://gitlab.com/sequoia-pgp/sequoia-sop"
 SOP-OPENPGPJS:
   title: "sop-openpgp.js"
   author:
    -
     org: Proton
   target: "https://github.com/openpgpjs/sop-openpgpjs"
 SOPGPY:
   title: "sopgpy"
   author:
    -
     name: Daniel Kahn Gillmor
   target: "https://github.com/SecurityInnovation/PGPy/pull/440"
--- abstract

This document defines a generic stateless command-line interface for dealing with OpenPGP messages, certificates, and secret key material, known as `sop`.
It aims for a minimal, well-structured API covering OpenPGP object security and maintenance of credentials and secrets.

--- middle

# Introduction

Different OpenPGP implementations have many different requirements, which typically break down in two main categories: key/certificate management and object security.

The purpose of this document is to provide a "stateless" interface that can handle both object security side and key and certificate management in a way that would be usable by applications across the full OpenPGP lifecycle.

A priority for this interface is to facilitate interoperability testing for OpenPGP implementations, for example as described in {{test-suite}}.

This document defines a generic stateless command-line interface for dealing with OpenPGP messages, secret keys, and certificates, known here by the placeholder `sop`.
It aims for a minimal, well-structured API.

An OpenPGP implementation should not name its executable `sop` to implement this specification.  It just needs to provide a program that conforms to this interface.

A `sop` implementation should leave no trace on the system, and its behavior should not be affected by anything other than command-line arguments and input.

Inputs to `sop` are immutable inputs.
Any named files that it receives as input should only need read access, and it must not write to or modify any of its inputs.
The only places a `sop` implementation should write to are standard output and (in some special cases) a location specified by an `--*-out=` argument.

Obviously, the user (or consuming application) will need to manage persistent secret keys and certificates somehow,
but the goal of this interface is to separate out that task from the task of processing and handling OpenPGP objects.

While this document identifies a command-line interface,
the rough outlines of this interface should also be amenable to relatively straightforward library implementations in different languages.

{{libsop}} offers a preliminary sketch of a C library interface that also has no implicit state.

## Requirements Language

{::boilerplate bcp14-tagged}

## Terminology

This document uses the term "key" to refer exclusively to OpenPGP Transferable Secret Keys (see {{Section 10.2 of !RFC9580}}).

It uses the term "certificate" to refer to OpenPGP Transferable Public Key (see {{Section 10.1 of RFC9580}}).

"Stateless" in "Stateless OpenPGP" means avoiding any sort of persistent and implicit state.
The user is responsible for managing all OpenPGP certificates and secret keys themselves,
and passing them to `sop` as needed.
The user should also not be concerned that any state could affect the underlying operations.

OpenPGP revocations can have "Reason for Revocation" ({{Section 5.2.3.31 of RFC9580}}), which can be either "soft" or "hard".
The set of "soft" reasons is: "Key is superseded" and "Key is retired and no longer used".
All other reasons (and revocations that do not state a reason) are "hard" revocations.

This document refers to a special verification-only subset of the `sop` command-line interface as `sopv` (see {{sopv}} for more details).

## Using sop in a Test Suite {#test-suite}

If an OpenPGP implementation provides a `sop` interface, it can be used to test interoperability (e.g., [OpenPGP-Interoperability-Test-Suite]).

Such an interop test suite can, for example, use custom code (*not* `sop`) to generate a new OpenPGP object that incorporates new primitives, and feed that object to a stable of `sop` implementations, to determine whether those implementations can consume the new form.

Or, the test suite can drive each `sop` implementation with a simple input, and observe which cryptographic primitives each implementation chooses to use as it produces output.

A simple self-test can be found in {{simple-self-test}}.

## Semantics vs. Wire Format

The semantics of `sop` are deliberately simple and very high-level compared to the vast complexity and nuance available within the OpenPGP specification.
This reflects the perspective of nearly every piece of tooling that relies on OpenPGP to accomplish its task: most toolchains don't care about the specifics, they just want the high-level object security properties.

Given this framing, this document generally tries to avoid overconstraining the details of the wire format objects emitted, or what kinds of wire format structures should be acceptable or unacceptable.
This allows a test suite to evaluate and contrast the wire format choices made by different implementations in as close to their native configuration as possible.
It also makes it easier to promote interoperability by ensuring that the native wire formats emitted by one implementation can be consumed by another, without relying on their choices of wire format being constrained by this draft.

Where this draft does identify specific wire format requirements, that might be due to an ambiguity in the existing specifications (which maybe needs fixing elsewhere), or to a bug in this specification that could be improved.

# Examples

These examples show no error checking, but give a flavor of how `sop` might be used in practice from a shell.

The key and certificate files described in them (e.g., `alice.sec`) could be for example those found in {{?I-D.draft-bre-openpgp-samples-01}}.

~~~
sop generate-key "Alice Lovelace <alice@openpgp.example>" > alice.sec
sop extract-cert < alice.sec > alice.pgp

sop generate-key "Bob Babbage <bob@openpgp.example>" > bob.sec
sop extract-cert < bob.sec > bob.pgp

sop sign --as=text alice.sec < statement.txt > statement.txt.asc
sop verify statement.txt.asc alice.pgp < statement.txt

sop encrypt --sign-with=alice.sec bob.pgp < msg.eml > ciphertext.asc
sop decrypt bob.sec < ciphertext.asc > cleartext.eml
~~~

See {{failure-modes}} for more information about errors and error handling.

# `sopv` Subset {#sopv}

While the primary goal of this document is to provide a full `sop` interface, as a special case, an implementer may choose to produce a version of the command-line interface that only supports OpenPGP signature verification.
As a shorthand, this document refers to such an interface as `sopv`, or "the `sopv` subset".
This can be useful for constrained environments where the only thing needed is signature verification, for example, system installation or update media.

A full implementation of `sop` by definition provides `sopv`, of course.

Only the following subcommands and their associated options MUST be implemented for `sopv`:

- `version` ({{version}})
- `verify` ({{verify}})
- `inline-verify` ({{inline-verify}})

## `sopv` Versioning

The abstract `sopv` interface is itself versioned using {{SEMVER}}.
The definition of the relevant subcommands and options specified in this document is known as `sopv` version 1.0.

If backward-incompatible changes are made to the `sopv` subset, the major version number will be increased.
If the `sopv` subset is extended without backward-incompatible changes, the minor version number will be increased.

Elements of the CLI relevant to `sopv` are annotated in this document with the `sopv` version in which they were introduced.

See also {{sopv-changelog}} for enumerated version history.

# Universal Options

Every invocation of `sop` or `sopv` MAY use the options described in this section, even though they are not specified in the synopsis for any specific subcommand.

## --debug: emit more verbose output

When the `--debug` option is present, `sop` MAY emit implementation-specific debugging information to standard error.

A locale-aware, internationalized `sop` implementation will localize this debugging information.

# Subcommands

`sop` uses a subcommand interface, similar to those popularized by systems like `git` and `svn`.

If the user supplies a subcommand that `sop` does not implement, it fails with `UNSUPPORTED_SUBCOMMAND`.
If a `sop` implementation does not handle a supplied option for a given subcommand, it fails with `UNSUPPORTED_OPTION`.

All subcommands that produce OpenPGP material on standard output produce ASCII-armored ({{Section 6 of !RFC9580}}) objects by default (except for `sop dearmor`).
These subcommands have a `--no-armor` option, which causes them to produce binary OpenPGP material instead.

All subcommands that accept OpenPGP material on input should be able to accept either ASCII-armored or binary inputs (see {{optional-input-armoring}}) and behave accordingly.

See {{indirect-types}} for details about how various forms of OpenPGP material are expected to be structured.

## Meta Subcommands

The subcommands grouped in this section are related to the `sop` implementation itself.

### version: Version Information {#version}

    sop version [--backend|--extended|--sop-spec|--sopv]

- Standard Input: ignored
- Standard Output: version information

This subcommand emits version information as UTF-8-encoded text.

With no arguments, the version string emitted should contain the name of the `sop` implementation, followed by a single space, followed by the version number.
A `sop` implementation should use a version number that respects an established standard that is easily comparable and parsable, like {{SEMVER}}.

If `--backend` is supplied, the implementation should produce a comparable line of implementation and version information about the primary underlying OpenPGP toolkit.

If `--extended` is supplied, the implementation may emit multiple lines of version information.
The first line MUST match the information produced by a simple invocation, but the rest of the text has no defined structure.

If `--sop-spec` is supplied, the implementation should emit a single line of text indicating the latest version of this draft that it targets, for example, `draft-dkg-openpgp-stateless-cli-06`.
If the implementation targets a specific draft but the implementer knows the implementation is incomplete, it should prefix the draft title with a <u>~</u>, for example: `~draft-dkg-openpgp-stateless-cli-06`.
The implementation MAY emit additional text about its relationship to the targeted draft on the lines following the versioned title.

If `--sopv` is supplied, the implementation should produce a single line with the implemented {{SEMVER}} version of the `sopv` interface subset (see {{sopv}}) that this implementation provides complete coverage for.
If the implementation does not provide complete coverage for any `sopv` interface, it should emit nothing on standard out and return `UNSUPPORTED_OPTION`.

`--backend`, `--extended`, `--sop-spec`, and `--sopv` are mutually-exclusive options.

Example:

    $ sop version
    ExampleSop 0.2.1
    $ sop version --backend
    LibExamplePGP 3.4.2
    $ sop version --extended
    ExampleSop 0.2.1
    Running on MonkeyScript 4.5
    LibExamplePGP 3.4.2
    LibExampleCrypto 3.1.1
    LibXCompression 4.0.2
    See https://pgp.example/sop/ for more information
    $ sop version --sop-spec
    ~draft-dkg-openpgp-stateless-cli-06
    
    This implementation does not handle @FD: special designators for output.
    $ sop version --sopv
    1.0
    $

The `version` subcommand and all of its options are part of the `sopv` subset (see {{sopv}}) starting at `sopv` version 1.0.

### list-profiles: Describe Available Profiles {#list-profiles}

    sop list-profiles SUBCOMMAND

- Standard Input: ignored
- Standard Output: `PROFILELIST` ({{profilelist}})

This subcommand emits a list of profiles supported by the identified subcommand.
The first profile listed is the default profile, as described in {{profilelist}}.

If the indicated `SUBCOMMAND` does not accept a `--profile` option, it returns `UNSUPPORTED_PROFILE`.

Example:

    $ sop list-profiles generate-key
    default: use the implementer's recommendations
    security: higher-security, maybe reduced performance
    performance: higher-performance, maybe reduced security
    rfc4880: use algorithms from RFC 4880 (alias: compatibility)
    $

## Key and Certificate Management Subcommands

The subcommands grouped in this section are primarily intended to manipulate keys and certificates.

### generate-key: Generate a Secret Key {#generate-key}

    sop generate-key [--no-armor]
        [--with-key-password=PASSWORD]
        [--profile=PROFILE]
        [--signing-only]
        [--] [USERID...]

 - Standard Input: ignored
 - Standard Output: `KEYS` ({{keys}})

Generate a single default OpenPGP key with zero or more User IDs.

The generated secret key SHOULD be usable for as much of the `sop` functionality as possible.
In particular:

 - It should be possible to extract an OpenPGP certificate from the key in `KEYS` with `sop extract-cert`.
 - The key in `KEYS` should be able to create signatures (with `sop sign`) that are verifiable by using `sop verify` with the extracted certificate.
 - Unless the `--signing-only` parameter is supplied, the key in `KEYS` should be able to decrypt messages (with `sop decrypt`) that are encrypted by using `sop encrypt` with the extracted certificate.

The detailed internal structure of the certificate is left to the discretion of the `sop` implementation.

If the `--with-key-password` option is supplied, the generated key will be password-protected (locked) with the supplied password.
Note that `PASSWORD` is an indirect data type from which the actual password is acquired ({{indirect-types}}).
See also the guidance on ensuring that the password is human-readable in {{generating-human-readable}}.

If no `--with-key-password` option is supplied, the generated key will be unencrypted.

If the `--profile` argument is supplied and the indicated `PROFILE` is not supported by the implementation, `sop` will fail with `UNSUPPORTED_PROFILE`.

The presence of the `--signing-only` option is intended to create a key that is only capable of signing, not decrypting.
This is useful for deployments where only signing and verification are necessary.

If any of the `USERID` options are not valid `UTF-8`, `sop generate-key` fails with `EXPECTED_TEXT`.

If the implementation rejects any `USERID` option that is valid `UTF-8` (e.g., due to internal policy, see {{userid}}), `sop generate-key` fails with `BAD_DATA`.

Example:

    $ sop generate-key 'Alice Lovelace <alice@openpgp.example>' > alice.sec
    $ head -n1 < alice.sec
    -----BEGIN PGP PRIVATE KEY BLOCK-----
    $

### change-key-password: Update a Key's Password {#change-key-password}

    sop change-key-password [--no-armor]
        [--new-key-password=PASSWORD]
        [--old-key-password=PASSWORD...]

 - Standard Input: `KEYS` ({{keys}})
 - Standard Output: `KEYS` ({{keys}})

The output will be the same set of OpenPGP Transferable Secret Keys as the input, but with all secret key material locked according to the password indicated by the `--new-key-password` option (or, with no password at all, if `--new-key-password` is absent).
Note that `--old-key-password` can be supplied multiple times, and each supplied password will be tried as a means to unlock any locked key material encountered.
It will normalize a Transferable Secret Key to use a single password even if it originally had distinct passwords locking each of the subkeys.

If any secret key packet is locked but cannot be unlocked with any of the supplied `--old-key-password` arguments, this subcommand should fail with `KEY_IS_PROTECTED`.

Example:

    # adding a password to an unlocked key:
    $ sop change-key-password --new-key-password=@ENV:keypass < unlocked.key > locked.key
    # removing a password:
    $ sop change-key-password --old-key-password=@ENV:keypass < locked.key > unlocked.key
    # changing a password:
    $ sop change-key-password --old-key-password=@ENV:keypass --new-key-password=@ENV:newpass < locked.key > refreshed.key
    $

### revoke-key: Create a Revocation Certificate {#revoke-key}

    sop revoke-key [--no-armor]
        [--with-key-password=PASSWORD...]

 - Standard Input: `KEYS` ({{keys}})
 - Standard Output: `CERTS` ({{certs}})

Generate a revocation certificate for each Transferable Secret Key found.
See {{Section 10.1.2 of RFC9580}} for a discussion of common forms of revocation certificate.

Example:

    $ sop revoke-key < alice.key > alice-revoked.pgp
    $

### extract-cert: Extract a Certificate from a Secret Key {#extract-cert}

    sop extract-cert [--no-armor]

- Standard Input: `KEYS` ({{keys}})
- Standard Output: `CERTS` ({{certs}})

The output should contain one OpenPGP certificate in `CERTS` per OpenPGP Transferable Secret Key found in `KEYS`.
There is no guarantee what order the `CERTS` will be in.

`sop extract-cert` SHOULD work even if any of the keys in `KEYS` is password-protected.

Example:

    $ sop extract-cert < alice.sec > alice.pgp
    $ head -n1 < alice.pgp
    -----BEGIN PGP PUBLIC KEY BLOCK-----
    $

### update-key: Keep a Secret Key Up-To-Date {#update-key}

    sop update-key [--no-armor]
        [--signing-only]
        [--no-added-capabilities]
        [--with-key-password=PASSWORD...]
        [--merge-certs=CERTS...]

- Standard Input: `KEYS` ({{keys}})
- Standard Output: `KEYS` ({{keys}})

The input OpenPGP Transferable Secret Keys that arrive on standard input will be updated by the implementation, and their updated forms will be produced on standard output.
This update will "fix" everything that the implementation knows how to fix to bring each Transferable Secret Key up to reasonable modern practice.
Each Transferable Secret Key output must be capable of signing, and (unless `--signing-only` is provided) capable of decryption.
The primary key of each Transferable Secret Key will not be changed in any way that affects its fingerprint.

One important aspect of `sop update-key` is how it handles advertisement of support for various OpenPGP capabilities (algorithms, mechanisms, etc).
All capabilities that the implementation knows it does not support, or knows to be weak and/or deprecated MUST be removed from the output Transferable Secret Keys.
This includes unknown/deprecated flags in the Features subpacket, and any unknown/deprecated algorithm IDs in algorithm preferences subpackets.
For example, an implementation compliant with {{RFC9580}} will never emit a Transferable Secret Key with a Preferred Hash Preferences subpacket that explicitly indicates support for `MD5`, `RIPEMD160`, or `SHA1`.

If `--no-added-capabilities` is not present, then any capability that the implementation supports and encourages that was not advertised in the input Transferable Secret Key MAY be added to the advertisements in the output Transferable Secret Key.
If `--no-added-capabilities` is present, then new capabilities MUST NOT be added to the advertised sets during the update.

Beyond cleanup of the advertised capabilities, `--signing-only`, and `--no-added-capabilities`, the choice of exactly what updates to do are up to the implementation.
It is expected that an implementer will document and describe the specific considerations and updates they make for this operation.
It is acceptable to propagate any non-critical unknown subpackets from old self-signatures to the new, replacement self-signatures.

Possible updates might include:

- Refresh or replace any subkey approaching expiry.
- Refresh any self-signature (including cross-sigs) that is approaching expiry.
- Refresh any self-signature (including cross-sigs) that is made using weak or risky algorithms.
- Correct any mistaken 2-octet hash prefix found in a signature (see {{Section 5.2.3 of RFC9580}}).
- Ensure proof of "aliveness": if no self-signatures are more recent than some cutoff in the recent past, re-issue the same self-signatures.

If there is nothing to be updated because all the incoming Transferable Secret Keys are already in good shape, then the same set of Transferable Secret Keys will be emitted to standard output and `sop update-key` succeeds.

If any Transferable Secret Key cannot be fixed (for example, because its primary key uses a weak algorithm, or because the whole certificate is hard-revoked), `sop update-key` fails with `PRIMARY_KEY_BAD`, emits an explanation on stderr, and nothing on stdout.

If any secret key that needs to make a signature to update the key cannot be unlocked with any of the supplied `PASSWORD` objects, `sop update-key` fails with `KEY_IS_PROTECTED`, emits an explanation on stderr, and nothing on stdout.

If `--merge-certs` is supplied, and any of the `CERTS` objects correspond to the Transferable Secret Keys being updated, then any additional elements found in the corresponding `CERTS` are merged into the Transferable Secret Key before it is emitted.
This can be used, for example, to absorb a third-party certification into the Transferable Secret Key.

Example (keeping certificates fresh):

    $ sop update-key < alice.key > alice-updated.key
    $ mv alice-updated.key alice.key
    $ sop extract-cert < alice.key > alice.pgp
    $

Example (advertising the intersection of features supported by two Stateless OpenPGP implementations, rendered here as `sop1` and `sop2`):

    $ sop1 update-key < alice.key | sop2 update-key | \
      sop1 --no-added-capabilities update-key > alice-updated.key
    $ mv alice-updated.key alice.key
    $ sop1 extract-cert < alice.key > alice.pgp
    $

### merge-certs: Merge OpenPGP Certificates {#merge-certs}

    sop merge-certs [--no-armor]
        [--] CERTS [CERTS...]

- Standard Input: `CERTS` ({{certs}})
- Standard Output: `CERTS` ({{certs}})

The OpenPGP certificates on standard input will be produced on standard output, merged with the corresponding elements of any of the `CERTS` objects named on the command line.

This can be used, for example, to absorb a third-party certification into a certificate, or to update a certificate's feature advertisements without losing local annotations.

The certificates produced on standard output are only the certificates received on standard input.
If any certificate found via named command line parameters does not share a primary key with any standard input certificate, the certificate from the command line is ignored.

If any of the OpenPGP certificates on standard input share the same primary key, they are also merged and de-deduplicated on standard output.
If multiple OpenPGP certificates named on the command line share a primary key with one of the certificates on standard input, their certificate updates are cumulatively merged for output.

Example:

    $ sop merge-certs alice-certified-by-bob.pgp < alice.pgp > alice-updated.pgp
    $ mv alice-updated.pgp alice.pgp
    $

## User Identity Subcommands

The subcommands in this section handle OpenPGP user identities.
OpenPGP certificates contain cryptographic certifications which bind text-based "User IDs" to primary key material, which is in turn cryptographically bound to additional key material.

These subcommands are related to the network of cryptographic identity assertions that has traditionally been called the "Web of Trust".
Note also the similarity in structure between these subcommands and `sop sign` ({{sign}}) and `sop verify` ({{verify}})

### certify-userid: Certify OpenPGP Certificate User IDs {#certify-userid}

    sop certify-userid [--no-armor]
        --userid=USERID
        [--userid=USERID...]
        [--with-key-password=PASSWORD...]
        [--no-require-self-sig]
        [--] KEYS [KEYS...]

- Standard Input: `CERTS` ({{certs}})
- Standard Output: `CERTS` ({{certs}})

With each Transferable Secret Key in all `KEYS` objects, add a third-party certification to `CERTS` found on standard input, and emit the updated OpenPGP certificates (including the new certification(s)) on standard output.

If the caller does not specify at least one `--userid=USERID` option, `sop certify-userid` fails with `MISSING_ARG`.

If the certification-capable key of any Transferable Secret Key in `KEYS` is locked and cannot be unlocked by any of the supplied `PASSWORD`s, fail with `KEY_IS_PROTECTED`.

If any incoming `CERTS` object does not already have all of the specified User IDs as valid, self-signed User IDs, then `sop certify-userid` fails with `CERT_USERID_NO_MATCH`, unless `--no-require-self-sig` is supplied.

If `--no-require-self-sig` is supplied, then each incoming OpenPGP certificate will have each specified User ID added to it (if it did not have it already), and certified directly, regardless of self-signatures.
This may be useful for associating a certificate with a specific identity even in cases where the certificate does not itself advertise the identity.

If any key in the `KEYS` objects is not capable of producing a certification, `sop sign` will fail with `KEY_CANNOT_CERTIFY`.

Example:

    $ sop certify-userid --userid="Alice Lovelace <alice@openpgp.example>" \
      bob.key < alice.pgp > alice-signed-by-bob.pgp
    $

Example (adding a User ID to your own certificate):

    $ sop certify-userid --userid="Alice Lovelace <lovelace@business.example>" \
      alice.key < alice.pgp > alice-updated.pgp
    $ sop update-key --merge-certs alice-updated.pgp < alice.key > alice-updated.key
    $ mv alice-updated.key alice.key
    $ rm alice-updated.pgp
    $ sop extract-cert < alice.key > alice.pgp
    $

### validate-userid: Validate a User ID in an OpenPGP Certificate {#validate-userid}

    sop validate-userid
        [--addr-spec-only]
        [--validate-at=DATE]
        [--] USERID CERTS [CERTS...]

- Standard Input: `CERTS` ({{certs}})
- Standard Output: none

Given a set of authority OpenPGP certificates on the command line, succeed if and only if all OpenPGP certificates on standard input are correctly bound by at least one valid signature from one authority to the `USERID` in question.

If `--addr-spec-only` is present, then the `USERID` is treated as an e-mail address, and matched only against the e-mail address part of each correctly bound User ID.
The rest of each correctly bound User ID is ignored.
If any correctly bound User ID is not a conventional OpenPGP User ID, it will not match with `--addr-spec-only` at all.
Note that {{RFC9580}} (and {{?RFC4880}} and {{?RFC2440}} before them) mislabeled an OpenPGP User ID as a `name-addr`, but that is likely to be wrong.

If `--validate-at` is present, then evaluate the validity of the User ID at the specified time.
If `--validate-at` is not present (or if it is present with the literal value `now`), the User ID validity is evaluated at the current time.

If any OpenPGP certificate in the `CERTS` on standard input does not have a correctly bound User ID that matches `USERID`, `sop validate-userid` fails with `CERT_USERID_NO_MATCH`.

Example:

    $ if sop validate-userid "Alice Lovelace <alice@openpgp.example>" \
      bob.pgp < alice.pgp; then echo Good; fi
    Good
    $ if sop validate-userid --addr-spec-only "alice@openpgp.example" \
      bob.pgp < alice.pgp; then echo Good; fi
    Good
    $

## Messaging Subcommands

The subcommands in this section handle OpenPGP messages: encrypting, decrypting, signing, and verifying.

### sign: Create Detached Signatures {#sign}

    sop sign [--no-armor] [--micalg-out=MICALG]
         [--with-key-password=PASSWORD...]
         [--as={binary|text}] [--] KEYS [KEYS...]

- Standard Input: `DATA` ({{data}})
- Standard Output: `SIGNATURES` ({{signature}})

Exactly one signature will be made by each key in the supplied `KEYS` arguments.

`--as` defaults to `binary`.
If `--as=text` and the input `DATA` is not valid `UTF-8` ({{utf8}}), `sop sign` fails with `EXPECTED_TEXT`.

`--as=binary` SHOULD result in OpenPGP signatures of type 0x00 ("Signature of a binary document").
`--as=text` SHOULD result in OpenPGP signatures of type 0x01 ("Signature of a canonical text document").
See {{Section 5.2.1 of RFC4880}} for more details.

When generating PGP/MIME messages ({{!RFC3156}}), it is useful to know what digest algorithm was used for the generated signature.
When `--micalg-out` is supplied, `sop sign` emits the digest algorithm used to the specified `MICALG` file in a way that can be used to populate the `micalg` parameter for the Content-Type (see {{micalg}}).
If the specified `MICALG` file already exists in the filesystem, `sop sign` will fail with `OUTPUT_EXISTS`.
When `--micalg-out` is supplied, the `DATA` on standard input should already be in canonical text form (7-bit clean, CRLF line endings, no trailing whitespace), as specified in {{Section 3 of RFC3156}}.
If the incoming `DATA` does not already meet these requirements, `sop sign` will fail with `EXPECTED_TEXT`, regardless of any argument supplied for `--as`.
When `--micalg-out` is supplied, and multiple signatures are made but they do not all use the same digest algorithm, `sop sign` MUST emit the empty string to the designated `MICALG`.

If the signing key material in any key in the `KEYS` objects is password-protected, `sop sign` SHOULD try all supplied `--with-key-password` options to unlock the key material until it finds one that enables the use of the key for signing.
If none of the `PASSWORD` options unlock the key (or if no such option is supplied), `sop sign` will fail with `KEY_IS_PROTECTED`.
Note that `PASSWORD` is an indirect data type from which the actual password is acquired ({{indirect-types}}).
Note also the guidance for retrying variants of a non-human-readable password in {{consuming-passwords}}.

If any key in the `KEYS` objects is not capable of producing a signature, `sop sign` will fail with `KEY_CANNOT_SIGN`.

`sop sign` MUST NOT produce any extra signatures beyond those from `KEYS` objects supplied on the command line.

Example:

    $ sop sign --as=text alice.sec < message.txt > message.txt.asc
    $ head -n1 < message.txt.asc
    -----BEGIN PGP SIGNATURE-----
    $

### verify: Verify Detached Signatures {#verify}

    sop verify [--not-before=DATE] [--not-after=DATE]
        [--] SIGNATURES CERTS [CERTS...]

- Standard Input: `DATA` ({{data}})
- Standard Output: `VERIFICATIONS` ({{verifications}})

`--not-before` and `--not-after` indicate that signatures with dates outside certain range MUST NOT be considered valid.

`--not-before` defaults to the beginning of time.
Accepts the special value `-` to indicate the beginning of time (i.e., no lower boundary).

`--not-after` defaults to the current system time (`now`).
Accepts the special value `-` to indicate the end of time (i.e., no upper boundary).

`sop verify` only returns `OK` if at least one certificate included in any `CERTS` object made a valid signature in the time window specified over the `DATA` supplied.

For details about the valid signatures, the user MUST inspect the `VERIFICATIONS` output.

If no `CERTS` are supplied, `sop verify` fails with `MISSING_ARG`.

If no valid signatures are found, `sop verify` fails with `NO_SIGNATURE`.
In this case, `sop verify` MAY emit some human-readable explanation to standard error about why no valid signatures were found, see {{explaining-non-verification}}.

See {{signature-verification}} for more details about signature verification.

Example:

(In this example, we see signature verification succeed first, and then fail on a modified version of the message.)

    $ sop verify message.txt.asc alice.pgp < message.txt
    2019-10-29T18:36:45Z EB85BB5FA33A75E15E944E63F231550C4F47E38E EB85BB5FA33A75E15E944E63F231550C4F47E38E mode:text {"signers": ["alice.pgp"]}
    $ echo $?
    0
    $ tr a-z A-Z < message.txt | sop verify message.txt.asc alice.pgp
    $ echo $?
    3
    $

The `verify` subcommand and all of its options are part of the `sopv` subset (see {{sopv}}) starting at `sopv` version 1.0.

### encrypt: Encrypt a Message {#encrypt}

    sop encrypt [--as={binary|text}]
        [--no-armor]
        [--with-password=PASSWORD...]
        [--sign-with=KEYS...]
        [--with-key-password=PASSWORD...]
        [--profile=PROFILE]
        [--session-key-out=SESSIONKEY]
        [--] [CERTS...]

- Standard Input: `DATA` ({{data}})
- Standard Output: `CIPHERTEXT` ({{ciphertext}})

`--as` defaults to `binary`.
The setting of `--as` corresponds to the one octet format field found in the Literal Data packet at the core of the output `CIPHERTEXT`.
If `--as` is set to `binary`, the octet is `b` (`0x62`).
If it is `text`, the format octet is `u` (`0x75`).

`--with-password` enables symmetric encryption (and can be used multiple times if multiple passwords are desired).

`--sign-with` creates exactly one signature by for each secret key found in the supplied `KEYS` object (this can also be used multiple times if signatures from keys found in separate files are desired).
If any key in any supplied `KEYS` object is not capable of producing a signature, `sop sign` will fail with `KEY_CANNOT_SIGN`.
If any signing key material in any supplied `KEYS` object is password-protected, `sop encrypt` SHOULD try all supplied `--with-key-password` options to unlock the key material until it finds one that enables the use of the key for signing.
If none of the `--with-key-password=PASSWORD` options can unlock any locked signing key material (or if no such option is supplied), `sop encrypt` will fail with `KEY_IS_PROTECTED`.
All signatures made must be placed inside the encryption produced by `sop encrypt`.

Note that both `--with-password` and `--with-key-password` supply `PASSWORD` arguments, but they do so in different contexts which are not interchangeable.
A `PASSWORD` supplied for symmetric encryption (`--with-password`) MUST NOT be used to try to unlock a signing key (`--with-key-password`) and a `PASSWORD` supplied to unlock a signing key MUST NOT be used to symmetrically encrypt the message.
Regardless of context, each `PASSWORD` argument is presented as an indirect data type from which the actual password is acquired ({{indirect-types}}).
If `sop encrypt` encounters a password which is not a valid `UTF-8` string ({{utf8}}), or is otherwise not robust in its representation to humans,
it fails with `PASSWORD_NOT_HUMAN_READABLE`.
If `sop encrypt` sees trailing whitespace at the end of a password,
it will trim the trailing whitespace before using the password.
See {{human-readable-passwords}} for more discussion about passwords.

If `--as` is set to `binary`, then `--sign-with` will sign as a binary document (OpenPGP signature type `0x00`).

If `--as` is set to `text`, then `--sign-with` will sign as a canonical text document (OpenPGP signature type `0x01`).
In this case, if the input `DATA` is not valid `UTF-8`  ({{utf8}}), `sop encrypt` fails with `EXPECTED_TEXT`.

If `--sign-with` is supplied for input `DATA` that is not valid `UTF-8`, `sop encrypt` MAY sign as a binary document (OpenPGP signature type `0x00`).

`sop encrypt` MUST NOT produce any extra signatures beyond those from `KEYS` objects identified by `--sign-with`.

The resulting `CIPHERTEXT` should be decryptable by the secret keys corresponding to every certificate included in all `CERTS`, as well as each password given with `--with-password`.

If no `CERTS` or `--with-password` options are present, `sop encrypt` fails with `MISSING_ARG`.

If at least one of the identified certificates requires encryption to an unsupported asymmetric algorithm, `sop encrypt` fails with `UNSUPPORTED_ASYMMETRIC_ALGO`.

If at least one of the identified certificates is not encryption-capable (e.g., revoked, expired, no encryption-capable flags on primary key and valid subkeys), `sop encrypt` fails with `CERT_CANNOT_ENCRYPT`.

If the `--profile` argument is supplied and the indicated `PROFILE` is not supported by the implementation, `sop` will fail with `UNSUPPORTED_PROFILE`.
The use of a profile for this subcommand allows an implementation faced with parametric or algorithmic choices to make a decision coarsely guided by the operator.
For example, when encrypting with a password, there is no knowledge about the capabilities of the recipient, and an implementation may prefer cryptographically modern algorithms, or it may prefer more broad compatibility.
In the event that a known recipient (i.e., one of the `CERTS`) explicitly indicates a lack of support for one of the features preferred by the indicated profile, the implementation SHOULD conform to the recipient's advertised capabilities where possible.

If `--session-key-out` argument is supplied, the session key generated for this encrypted will be written to the indicated location.
This can be useful, for example, when Alice encrypts a message to Bob, but also wants to retain the ability to read it without having any of her own secret key material available (see {{Section 9.1 of ?I-D.ietf-lamps-e2e-mail-guidance-11}}).

If `sop encrypt` fails for any reason, it emits no `CIPHERTEXT`.

Example:

(In this example, `bob.bin` is a file containing Bob's binary-formatted OpenPGP certificate.
Alice is encrypting a message to both herself and Bob.)

    $ sop encrypt --as=text --sign-with=alice.key alice.asc bob.bin < message.eml > encrypted.asc
    $ head -n1 encrypted.asc
    -----BEGIN PGP MESSAGE-----
    $

### decrypt: Decrypt a Message {#decrypt}

    sop decrypt [--session-key-out=SESSIONKEY]
        [--with-session-key=SESSIONKEY...]
        [--with-password=PASSWORD...]
        [--with-key-password=PASSWORD...]
        [--verifications-out=VERIFICATIONS
         [--verify-with=CERTS...]
         [--verify-not-before=DATE]
         [--verify-not-after=DATE] ]
        [--] [KEYS...]

- Standard Input: `CIPHERTEXT` ({{ciphertext}})
- Standard Output: `DATA` ({{data}})

The caller can ask `sop` for the session key discovered during decryption by supplying the `--session-key-out` option.
If the specified file already exists in the filesystem, `sop decrypt` will fail with `OUTPUT_EXISTS`.
When decryption is successful, `sop decrypt` writes the discovered session key to the specified file.

`--with-session-key` enables decryption of the `CIPHERTEXT` using the session key directly against the `SEIPD` packet.
This option can be used multiple times if several possible session keys should be tried.
`SESSIONKEY` is an indirect data type from which the actual `sessionkey` value is acquired ({{indirect-types}}).

`--with-password` enables decryption based on any `SKESK` ({{Section 5.3 of RFC9580}}) packets in the `CIPHERTEXT`.
This option can be used multiple times if the user wants to try more than one password.

`--with-key-password` lets the user use password-protected (locked) secret key material.
If the decryption-capable secret key material in any key in the `KEYS` objects is password-protected, `sop decrypt` SHOULD try all supplied `--with-key-password` options to unlock the key material until it finds one that enables the use of the key for decryption.
If none of the `--with-key-password` options unlock the key (or if no such option is supplied), and the message cannot be decrypted with any other `KEYS`, `--with-session-key`, or `--with-password` options, `sop decrypt` will fail with `KEY_IS_PROTECTED`.

Note that the two kinds of `PASSWORD` options are for different domains: `--with-password` is for unlocking an `SKESK`, and `--with-key-password` is for unlocking secret key material in `KEYS`.
`sop decrypt` SHOULD NOT apply the `--with-key-password` argument to any `SKESK`, or the `--with-password` argument to any `KEYS`.

Each `PASSWORD` argument is an indirect data type from which the actual password is acquired ({{indirect-types}}).
If `sop decrypt` tries and fails to use a password supplied by a `PASSWORD`,
and it observes that there is trailing `UTF-8` whitespace at the end of the password,
it will retry with the trailing whitespace stripped.
See {{consuming-passwords}} for more discussion about consuming password-protected key material.

`--verifications-out` produces signature verification status to the designated file.
If the designated file already exists in the filesystem, `sop decrypt` will fail with `OUTPUT_EXISTS`.

The return code of `sop decrypt` is not affected by the results of signature verification.
The caller MUST check the returned `VERIFICATIONS` to confirm signature status.
An empty `VERIFICATIONS` output indicates that no valid signatures were found.

If no valid signatures were found, but `--verifications-out` was supplied, `sop decrypt` MAY emit some human-readable explanation to standard error about why no valid signatures were found, see {{explaining-non-verification}}.

`--verify-with` identifies a set of certificates whose signatures would be acceptable for signatures over this message.

If the caller is interested in signature verification, both `--verifications-out` and at least one `--verify-with` must be supplied.
If only one of these options is supplied, `sop decrypt` fails with `INCOMPLETE_VERIFICATION`.

`--verify-not-before` and `--verify-not-after` provide a date range for acceptable signatures,
by analogy with the options for `sop verify` (see {{verify}}).
They should only be supplied when doing signature verification.

See {{signature-verification}} for more details about signature verification.

If no `KEYS` or `--with-password` or `--with-session-key` options are present, `sop decrypt` fails with `MISSING_ARG`.

If unable to decrypt, `sop decrypt` fails with `CANNOT_DECRYPT`.

`sop decrypt` only emits cleartext to Standard Output that was successfully decrypted.

Example:

(In this example, Alice stashes and re-uses the session key of an encrypted message.)

    $ sop decrypt --session-key-out=session.key alice.sec < ciphertext.asc > cleartext.out
    $ ls -l ciphertext.asc cleartext.out
    -rw-r--r-- 1 user user   321 Oct 28 01:34 ciphertext.asc
    -rw-r--r-- 1 user user   285 Oct 28 01:34 cleartext.out
    $ sop decrypt --with-session-key=session.key < ciphertext.asc > cleartext2.out
    $ diff cleartext.out cleartext2.out
    $

#### Historic Options for sop decrypt

The `sop decrypt` option `--verifications-out` used to be named `--verify-out`.
An implementation SHOULD accept either form of this option, and SHOULD produce a deprecation warning to standard error if the old form is used.

### inline-detach: Split Signatures from an Inline-Signed Message  {#inline-detach}

    sop inline-detach [--no-armor] --signatures-out=SIGNATURES

- Standard Input: `INLINESIGNED`
- Standard Output: `DATA` (the message without any signatures)

In some contexts, the user may expect an inline-signed message of some form or another (`INLINESIGNED`, see {{inlinesigned}}) rather than a message and its detached signature.
`sop inline-detach` takes such an inline-signed message on standard input, and splits it into:

 - the potentially signed material on standard output, and
 - a detached signature block to the destination identified by `--signatures-out`

Note that no cryptographic verification of the signatures is done by this subcommand.
Once the inline-signed message is separated, verification of the detached signature can be done with `sop verify`.

If no `--signatures-out` is supplied, `sop inline-detach` fails with `MISSING_ARG`.

Note that there may be more than one Signature packet in an inline-signed message.
All signatures found in the inline-signed message will be emitted to the `--signatures-out` destination.

If the inline-signed message uses the Cleartext Signature Framework, it may be dash-escaped (see {{Section 7.2 of RFC9580}}).
The output of `sop detach-inband-signature-and-message` will have any dash-escaping removed.

If the input is not an `INLINESIGNED` message, `sop inline-detach` fails with `BAD_DATA`.
If the input contains more than one object that could be interpreted as an `INLINESIGNED` message, `sop inline-detach` also fails with `BAD_DATA`.
A `sop` implementation MAY accept (and discard) leading and trailing data when the incoming `INLINESIGNED` message uses the Cleartext Signature Framework.

If the file designated by `--signatures-out` already exists in the filesystem, `sop detach-inband-signature-and-message` will fail with `OUTPUT_EXISTS`.

Note that `--no-armor` here governs the data written to the `--signatures-out` destination.
Standard output is always the raw message, not an OpenPGP packet.

Example:

    $ sop inline-detach --signatures-out=Release.pgp < InRelease >Release
    $ sop verify Release.pgp archive-keyring.pgp < Release
    $

### inline-verify: Verify an Inline-Signed Message {#inline-verify}

    sop inline-verify [--not-before=DATE] [--not-after=DATE]
        [--verifications-out=VERIFICATIONS]
        [--] CERTS [CERTS...]

- Standard Input: `INLINESIGNED` ({{inlinesigned}})
- Standard Output: `DATA` ({{data}})

This command is similar to `sop verify` ({{verify}}) except that it takes an `INLINESIGNED` message (see {{inlinesigned}}) and produces the message body (without signatures) on standard output.
It is also similar to `sop inline-detach` ({{inline-detach}}) except that it actually performs signature verification.

`--not-before` and `--not-after` indicate that signatures with dates outside certain range MUST NOT be considered valid.
See {{verify}} for their syntax and defaults.

`sop inline-verify` only returns `OK` if `INLINESIGNED` contains at least one valid signature made during the time window specified by a certificate included in any `CERTS` object.

For details about the valid signatures, the user MUST inspect the `VERIFICATIONS` output.

If no `CERTS` are supplied, `sop inline-verify` fails with `MISSING_ARG`.

If no valid signatures are found, `sop inline-verify` fails with `NO_SIGNATURE` and emits nothing on standard output.
In this case, `sop inline-verify` MAY emit some human-readable explanation to standard error about why no valid signatures were found, see {{explaining-non-verification}}.

See {{signature-verification}} for more details about signature verification.

Example:

(In this example, we see signature verification succeed first, and then fail on a modified version of the message.)

    $ sop inline-verify -- alice.pgp < message.txt
    Hello, world!
    $ echo $?
    0
    $ sed s/Hello/Goodbye/ < message.txt | sop inline-verify -- alice.pgp
    $ echo $?
    3
    $

The `inline-verify` subcommand and all of its options are part of the `sopv` subset (see {{sopv}}) starting at `sopv` version 1.0.

### inline-sign: Create an Inline-Signed Message {#inline-sign}

    sop inline-sign [--no-armor]
         [--with-key-password=PASSWORD...]
         [--as={binary|text|clearsigned}]
         [--] KEYS [KEYS...]

- Standard Input: `DATA` ({{data}})
- Standard Output: `INLINESIGNED` ({{inlinesigned}})

Exactly one signature will be made by each key in the supplied `KEYS` arguments.

The generated output stream will be an inline-signed message, by default producing an OpenPGP "Signed Message" packet stream.

`--as` defaults to `binary`.
If `--as=` is set to either `text` or `clearsigned`, and the input `DATA` is not valid `UTF-8` ({{utf8}}), `sop inline-sign` fails with `EXPECTED_TEXT`.

`--as=binary` SHOULD result in OpenPGP signatures of type 0x00 ("Signature of a binary document", see {{Section 5.2.1.1 of RFC9580}}).
`--as=text` SHOULD result in an OpenPGP signature of type 0x01 ("Signature of a canonical text document" see {{Section 5.2.1.2 of RFC9580}}).
`--as=clearsigned` SHOULD behave the same way as `--as=text` except that it produces an output stream using the Cleartext Signature Framework (see {{Section 7 of RFC9580}} and {{csf-risks}}).

If both `--no-armor` and `--as=clearsigned`  are supplied, `sop inline-sign` fails with `INCOMPATIBLE_OPTIONS`.

If the signing key material in any key in the `KEYS` objects is password-protected, `sop inline-sign` SHOULD try all supplied `--with-key-password` options to unlock the key material until it finds one that enables the use of the key for signing.
If none of the `PASSWORD` options unlock the key (or if no such option is supplied), `sop inline-sign` will fail with `KEY_IS_PROTECTED`.
Note that `PASSWORD` is an indirect data type from which the actual password is acquired ({{indirect-types}}).
Note also the guidance for retrying variants of a non-human-readable password in {{consuming-passwords}}.

If any key in the `KEYS` objects is not capable of producing a signature, `sop inline-sign` will fail with `KEY_CANNOT_SIGN`.

`sop inline-sign` MUST NOT produce any extra signatures beyond those from `KEYS` objects supplied on the command line.

Example:

    $ sop inline-sign --as=clearsigned alice.sec < message.txt > message-signed.txt
    $ head -n5 < message-signed.txt
    -----BEGIN PGP SIGNED MESSAGE-----
    Hash: SHA256
    
    This is the message.
    -----BEGIN PGP SIGNATURE-----
    $

## Transport Subcommands

The commands in this section handle manipulating OpenPGP objects for transport: armoring and dearmoring for 7-bit cleanness and compactness, respectively.

### armor: Convert Binary to ASCII

    sop armor

- Standard Input: OpenPGP material (`SIGNATURES`, `KEYS`, `CERTS`, `CIPHERTEXT`, or `INLINESIGNED`)
- Standard Output: the same material with ASCII-armoring added, if not already present

`sop armor` inspects the input and chooses the label appropriately, based on the OpenPGP packets encountered.

`sop armor` ought to be able to correctly re-armor any of the packet streams that are produced by `sop` with the `--no-armor` option.

For example, if the type of the first OpenPGP packet is:

- `0x05` (Secret-Key), the packet stream should be parsed as a `KEYS` input (with Armor Header `BEGIN PGP PRIVATE KEY BLOCK`).
- `0x06` (Public-Key), the packet stream should be parsed as a `CERTS` input (with Armor Header `BEGIN PGP PUBLIC KEY BLOCK`).
- `0x01` (Public-key Encrypted Session Key) or `0x03` (Symmetric-key Encrypted Session Key), the packet stream should be parsed as a `CIPHERTEXT` input (with Armor Header `BEGIN PGP MESSAGE`).
- `0x04` (One-Pass Signature), the packet stream should be parsed as an `INLINESIGNED` input (with Armor Header `BEGIN PGP MESSAGE`).
- `0x02` (Signature), the packet stream may be either a `SIGNATURES` input or an `INLINESIGNED` input.
  If the packet stream contains only Signature packets, it should be parsed as a`SIGNATURES` input (with Armor Header `BEGIN PGP SIGNATURE`).
  If it contains any packet other than a Signature packet, it should be parsed as an `INLINESIGNED` input (with Armor Header `BEGIN PGP MESSAGE`).

If the input packet stream does not match any expected sequence of packet types, `sop armor` fails with `BAD_DATA`.

Since `sop armor` accepts ASCII-armored input as well as binary input, this operation is idempotent on well-structured data.
A caller can use this subcommand blindly to ensure that any well-formed OpenPGP packet stream is 7-bit clean.

FIXME: what to do if the input is a CSF `INLINESIGNED` message?
Three choices:

- Leave it untouched -- this violates the claim about blindly ensuring 7-bit clean, since UTF-8-encoded message text is not necessarily 7-bit clean.
- Convert to ASCII-armored `INLINESIGNED` -- this requires synthesis of OPS packet (from signatures block) and Literal Data packet (from the message body).
- Raise a specific error.

Example:

    $ sop armor < bob.bin > bob.pgp
    $ head -n1 bob.pgp
    -----BEGIN PGP PUBLIC KEY BLOCK-----
    $

#### Historic Options for sop armor

`sop armor` used to be specified as having a `--label` option, with an argument that took one of the following values: `auto`, `sig`, `key`, `cert`, or `message`, which allowed the user to specify the label used in the header and tail of the armoring.

The default value for `--label` was `auto`, which matches the currently specified behavior.
This option is now deprecated, as it offers no useful functionality.

### dearmor: Convert ASCII to Binary

    sop dearmor

- Standard Input: OpenPGP material (`SIGNATURES`, `KEYS`, `CERTS`, `CIPHERTEXT`, or `INLINESIGNED`)
- Standard Output: the same material with any ASCII-armoring removed

If the input packet stream does not match any of the expected sequence of packet types, `sop dearmor` fails with `BAD_DATA`.  See also {{optional-input-armoring}}.

Since `sop dearmor` accepts binary-formatted input as well as ASCII-armored input, this operation is idempotent on well-structured data.
A caller can use this subcommand blindly ensure that any well-formed OpenPGP packet stream is in its standard binary representation.

FIXME: what to do if the input is a CSF `INLINESIGNED`?
Three choices:

- Leave it untouched -- output data is not really in binary format.
- Convert to binary-format `INLINESIGNED` -- this requires synthesis of OPS packet (from CSF `Hash` header) and Literal Data packet (from the message body).
- Raise a specific error.

Example:

    $ sop dearmor < message.txt.asc > message.txt.sig
    $

# Input String Types

Some material is passed to `sop` directly as a string on the command line.


## DATE {#date}

An ISO-8601 formatted timestamp with time zone, or the special value `now` to indicate the current system time.

Examples:

- `now`
- `2019-10-29T12:11:04+00:00`
- `2019-10-24T23:48:29Z`
- `20191029T121104Z`

In some cases where used to specify lower and upper boundaries, a `DATE` value can be set to `-` to indicate "no time limit".

A flexible implementation of `sop` MAY accept date inputs in other unambiguous forms.

Note that whenever `sop` emits a timestamp (e.g., in {{verifications}}) it MUST produce only a UTC-based ISO-8601 compliant representation with a resolution of one second, using the literal `Z` suffix to indicate timezone.

## USERID {#userid}

This is an arbitrary `UTF-8` string ({{utf8}}).
By convention, most User IDs are of the form `Display Name <email.address@example.com>`, but they do not need to be.

By internal policy, an implementation MAY reject a `USERID` if there are certain `UTF-8` strings it declines to work with as a User ID.
For example, an implementation may reject the empty string, or a string with characters in it that it considers problematic.
Of course, refusing to create a particular User ID does not prevent an implementation from encountering such a User ID in its input.

## SUBCOMMAND {#subcommand}

This is an `ASCII` string that matches the name of one of the subcommands listed in {{subcommands}}.

## PROFILE {#profile}

Some `sop` subcommands can accept a `--profile` option, which takes as an argument the name of a profile.

A profile name is a UTF-8 string that has no whitespace in it.

Which profiles are available depends on the `sop` implementation.

Similar to OpenPGP Notation names, profile names are divided into two namespaces: the IETF namespace and the user namespace.
A profile name in the user namespace ends with the `@` character (0x40) followed by a DNS domain name.
A profile name in the IETF namespace does not have an `@` character.

A profile name in the user space is owned and controlled by the owner of the domain in the suffix.
A `sop` implementation that implements a user profile but does not own the domain in question SHOULD hew as closely as possible to the semantics described by the owner of the domain.

A profile name in the IETF namespace that begins with the string `rfc` should have semantics that hew as closely as possible to the referenced RFC.
Similarly, a profile name in the IETF namespace that begins with the string `draft-` should have semantics that hew as closely as possible to the referenced Internet Draft.

The reserved profile name `default` in the IETF namespace simply refers to the implementation's default choices.
It is not mandatory to name the default profile `default`.
The first profile listed in the `list-profiles` output is considered the default configuration, as specified in {{profilelist}}.

The reserved profile names `security`, `performance`, and `compatibility` refer to the implementation's choices when increased emphasis on security, performance or compatibility is required, respectively.
It is not mandatory to name any profile `security`, `performance`, or `compatibility`; in that case, those profile names MUST act as aliases of another profile name.
They are also allowed to be aliases of the default profile.

Note that this profile mechanism is intended to provide a limited way for an implementation to select among a small set of options that the implementer has vetted and is satisfied with.
It is not intended to provide an arbitrary channel for complex configuration, and a `sop` implementation MUST NOT use it in that way.

# Input/Output Indirect Types {#indirect-types}

Some material is passed to `sop` indirectly, typically by referring to a filename containing the data in question.
This type of data may also be passed to `sop` on Standard Input, or delivered by `sop` to Standard Output.

If any input data is specified explicitly to be read from a file that does not exist, `sop` will fail with `MISSING_INPUT`.

If any input data does not meet the requirements described below, `sop` will fail with `BAD_DATA`.

## Special Designators for Indirect Types {#special-designators}

An indirect argument or parameter that starts with <u>@</u> is not treated as a filename, but is reserved for special handling, based on the prefix that follows the `@`.
We describe two of those prefixes (`@ENV:` and `@FD:`) here.
A `sop` implementation that receives such a special designator but does not know how to handle a given prefix in that context MUST fail with `UNSUPPORTED_SPECIAL_PREFIX`.

See {{special-designators-guidance}} for more details about safe handling of these special designators.

### @ENV: Special Designator for Environment Variable

If the filename for any indirect material used as input has the special form `@ENV:xxx`,
then contents of environment variable `$xxx` is used instead of looking in the filesystem.
`@ENV` is for input only: if the prefix `@ENV:` is used for any output argument, `sop` fails with `UNSUPPORTED_SPECIAL_PREFIX`.

The `sopv` subset (see {{sopv}}) MUST be capable of supporting the `@ENV` special designator for all relevant inputs starting at `sopv` version 1.0.

### @FD: Special Designator for File Descriptor

If the filename for any indirect material used as either input or output has the special form `@FD:nnn` where `nnn` is a decimal integer,
then the associated data is read from file descriptor `nnn`.

On platforms which support file descriptors, the `sopv` subset (see {{sopv}}) MUST be capable of supporting the `@FD` special designator for all relevant inputs and outputs starting at `sopv` version 1.0.

## CERTS {#certs}

One or more OpenPGP certificates ({{Section 10.1 of RFC9580}}), aka "Transferable Public Key".
May be armored (see {{optional-input-armoring}}).

Although some existing workflows may prefer to use one `CERTS` object with multiple certificates in it (a "keyring"), supplying exactly one certificate per `CERTS` input will make error reporting clearer and easier.

If any `CERTS` input contains secret key material, `sop` MUST fail with `BAD_DATA`.
This strictness is intended to keep the consumer of the `sop` interface clear about what material they are dealing with in what locations.
This should reduce the consumer's risk of accidentally exposing secret key material where they meant to expose a `CERTS` object.

## KEYS {#keys}

One or more OpenPGP Transferable Secret Keys ({{Section 10.2 of RFC9580}}).
May be armored (see {{optional-input-armoring}}).

Secret key material is often locked with a password to ensure that it cannot be simply copied and reused.
If any secret key material is locked with a password and no `--with-key-password` option is supplied, `sop` may fail with error `KEY_IS_PROTECTED`.
However, when a cleartext secret key (that is, one not locked with a password) is available, `sop` should always be able to use it, whether a `--with-key-password` option is supplied or not.

Although some existing workflows may prefer to use one `KEYS` object with multiple keys in it (a "secret keyring"), supplying exactly one key per `KEYS` input will make error reporting clearer and easier.

## CIPHERTEXT {#ciphertext}

`sop` accepts only a restricted subset of the arbitrarily-nested grammar allowed by the OpenPGP Messages definition ({{Section 10.3 of RFC9580}}).

In particular, it accepts and generates only:

An OpenPGP message, consisting of a sequence of PKESKs ({{Section 5.1 of RFC9580}}) and SKESKs ({{Section 5.3 of RFC9580}}),
followed by one SEIPD ({{Section 5.13 of RFC9580}}).

The SEIPD can decrypt into one of two things:

- "Maybe Signed Data" (see below), or

- Compressed data packet that contains "Maybe Signed Data"

"Maybe Signed Data" is a sequence of:

- N (zero or more) one-pass signature packets, followed by
- zero or more signature packets, followed by
- one Literal data packet, followed by
- N signature packets (corresponding to the outer one-pass signatures packets)

FIXME: does any tool do compression inside signing?  Do we need to handle that?

May be armored (see {{optional-input-armoring}}).

## INLINESIGNED {#inlinesigned}

An inline-signed message may take any one of three different forms:

- A binary sequence of OpenPGP packets that matches a subset of the "Signed Message" element in the grammar in {{Section 10.3 of RFC9580}}
- The same sequence of packets, but ASCII-armored (see {{optional-input-armoring}})
- A message using the Cleartext Signature Framework described in {{Section 7 of RFC9580}}

The subset of the packet grammar expected in the first two forms consists of either:

- a series of Signature packets followed by a Literal Data packet
- a series of One-Pass Signature (OPS) packets, followed by one Literal Data packet, followed by an equal number of Signature packets corresponding to the OPS packets

When the message is in the third form (Cleartext Signature Framework), it has the following properties:

- The stream SHOULD consist solely of `UTF-8` text
- Every Signature packet found in the stream SHOULD have Signature Type 0x01 (canonical text document).
- It SHOULD NOT contain leading text (before the `-----BEGIN PGP SIGNED MESSAGE-----` cleartext header) or trailing text (after the `-----END PGP SIGNATURE-----` armor tail).

While some OpenPGP implementations MAY produce more complicated inline signed messages, a `sop` implementation SHOULD limit itself to producing these straightforward forms.

## SIGNATURES {#signature}

One or more OpenPGP Signature packets.  May be armored (see {{optional-input-armoring}}).

## SESSIONKEY {#sessionkey}

This documentation uses the GnuPG defacto `ASCII` representation:

`ALGONUM:HEXKEY`

where `ALGONUM` is the decimal value associated with the OpenPGP Symmetric Key Algorithms ({{Section 9.3 of RFC9580}}) and `HEXKEY` is the hexadecimal
representation of the binary key.

Example AES-256 session key:

    9:FCA4BEAF687F48059CACC14FB019125CD57392BAB7037C707835925CBF9F7BCD

A `sop` implementation SHOULD produce session key data in this format.
When consuming such a session key, `sop` SHOULD be willing to accept either upper or lower case hexadecimal digits, and to gracefully ignore any trailing whitespace.

## MICALG {#micalg}

This output-only type indicates the cryptographic digest used when making a signature.
It is useful specifically when generating signed PGP/MIME objects, which want a `micalg=` parameter for the `multipart/signed` content type as described in {{Section 5 of RFC3156}}.

It will typically be a string like `pgp-sha512`, but in some situations (multiple signatures using different digests) it will be the empty string.
If the user of `sop` is assembling a PGP/MIME signed object, and the `MICALG` output is the empty string,
the user should omit the `micalg=` parameter entirely.

## PASSWORD {#password}

This input-only is expected to be a `UTF-8` string ({{utf8}}), but for `sop decrypt`, any bytestring that the user supplies will be accepted.
Note the details in `sop encrypt` and `sop decrypt` about trailing whitespace!

See also {{human-readable-passwords}} for more discussion.

## VERIFICATIONS {#verifications}

This output-only type consists of one line per successful signature verification.
Each line has four structured fields delimited by a single space,
followed by a single-line JSON object or arbitrary text to the end of the line.

- ISO-8601 UTC datestamp of the signature, to one second precision, using the `Z` suffix
- Fingerprint of the signing key (may be a subkey)
- Fingerprint of primary key of signing certificate (if signed by primary key, same as the previous field)
- A string describing the mode of the signature, either `mode:text` or `mode:binary`
- A JSON object or free-form message describing the verification (see {{verifications-json}})

Note that while {{date}} permits a `sop` implementation to accept other unambiguous date representations,
its date output here MUST be a strict ISO-8601 UTC date timestamp.
In particular:

- the date and time fields MUST be separated by `T`, not by whitespace, since whitespace is used as a delimiter
- the time MUST be emitted in UTC, with the explicit suffix `Z`
- the time MUST be emitted with one-second precision

Example:

    2019-10-24T23:48:29Z C90E6D36200A1B922A1509E77618196529AE5FF8 C4BC2DDB38CCE96485EBE9C2F20691179038E5C6 mode:binary {"signers": ["dkg.asc"]}

### VERIFICATIONS extension JSON {#verifications-json}

The final field of each `VERIFICATIONS` line is either JSON data or arbitrary text.

If the final field begins and ends with curly brackets (<u>{</u> and <u>}</u>, it is JSON data.
Otherwise, the final field is arbitrary text (whose content and structure are up to the discretion of the implementation).

JSON data allows for sophisticated future extensions, and is the preferred form of this field.
Arbitrary text is deprecated.
The rest of this subsection describes the JSON data.

The JSON data is a single JSON object, coerced into a one-line representation (there are no literal LINE FEED (U+000A) characters in it, though there may be appropriately escaped LINE FEED characters within the JSON text).

The JSON object MAY contain the following keys:

- `signers`: a list the supplied `CERTS` objects that could have issued the signature, identified by the name given on the command line.
  If this key is present, as long as any OpenPGP certificate in a given `CERTS` object could have issued the signature, that `CERTS` object MUST be listed here.
  If multiple `CERTS` objects contain certificates that could have issued the signature, each `CERTS` MUST be listed here.
- `comment`: Free-form UTF-8-encoded text describing the verification.
  An internationalized, locale-aware `sop` implementation should localize this field.
- `ext`: A "extensions" JSON object containing arbitrary, implementation-specific data.

To avoid collisions with future definitions, the top-level JSON object MUST NOT contain any other keys.
For forward compatibility, when consuming a JSON object produced by a SOP implementation, unknown keys MUST be ignored.

## DATA {#data}

Cleartext, arbitrary data.  This is either a bytestream or `UTF-8` text.

It MUST only be `UTF-8` text in the case of input supplied to `sop sign --as=text` or `sop encrypt --as=text`.
If `sop` receives `DATA` containing non-`UTF-8` octets in this case, it will fail (see {{utf8}}) with `EXPECTED_TEXT`.

## PROFILELIST {#profilelist}

This output-only type consists of simple UTF-8 textual output, with one line per profile.
Each line consists of the profile name optionally followed by a colon (0x31), a space (0x20), and a brief human-readable description of the intended semantics of the profile.
Each line may be at most 1000 bytes, and no more than 4 profiles may be listed.

These limits are intended to force `sop` implementers to make hard decisions and to keep things simple.

The first profile MAY be explicitly named `default`.
If it is not named `default`, then `default` is an alias for the first profile listed.
No profile after the first listed may be named `default`.

Any of the profiles MAY be explicitly named `security`, `performance`, or `compatibility`.
If none of the listed profiles have (some of) these names, the profiles of which they are an alias should indicate as much in the human-readable description.

See {{profile}} for more discussion about the namespace and intended semantics of each profile.

# Failure Modes {#failure-modes}

`sop` return codes have both mnemonics and numeric values.

When `sop` succeeds, it will return 0 (`OK`) and emit nothing to Standard Error.
When `sop` fails, it fails with a non-zero return code, and emits one or more warning messages on Standard Error.
Known return codes include:

{: title="Error return codes"}
Value | Mnemonic | Meaning
---:|----|----------------------------------------------
 0 | `OK` | Success
 1 | `UNSPECIFIED_FAILURE` | An otherwise unspecified failure occurred
 3 | `NO_SIGNATURE` | No acceptable signatures found (`sop verify`)
13 | `UNSUPPORTED_ASYMMETRIC_ALGO` | Asymmetric algorithm unsupported (`sop encrypt`)
17 | `CERT_CANNOT_ENCRYPT` | Certificate not encryption-capable (e.g., expired, revoked, unacceptable usage flags) (`sop encrypt`)
19 | `MISSING_ARG` | Missing required argument
23 | `INCOMPLETE_VERIFICATION` | Incomplete verification instructions (`sop decrypt`)
29 | `CANNOT_DECRYPT` | Unable to decrypt (`sop decrypt`)
31 | `PASSWORD_NOT_HUMAN_READABLE` | Non-`UTF-8` or otherwise unreliable password (`sop encrypt`, `sop generate-key`)
37 | `UNSUPPORTED_OPTION` | Unsupported option
41 | `BAD_DATA` | Invalid data type (no secret key where `KEYS` expected, secret key where `CERTS` expected, etc)
53 | `EXPECTED_TEXT` | Non-text input where text expected
59 | `OUTPUT_EXISTS` | Output file already exists
61 | `MISSING_INPUT` | Input file does not exist
67 | `KEY_IS_PROTECTED` | A `KEYS` input is password-protected (locked), and `sop` cannot unlock it with any of the `--with-key-password` (or `--old-key-password`) options
69 | `UNSUPPORTED_SUBCOMMAND` | Unsupported subcommand
71 | `UNSUPPORTED_SPECIAL_PREFIX` | An indirect parameter is a special designator (it starts with `@`) but `sop` does not know how to handle the prefix
73 | `AMBIGUOUS_INPUT` | A indirect input parameter is a special designator (it starts with `@`), and a filename matching the designator is actually present
79 | `KEY_CANNOT_SIGN` | Key not signature-capable (e.g., expired, revoked, unacceptable usage flags) (`sop sign` and `sop encrypt` with `--sign-with`)
83 | `INCOMPATIBLE_OPTIONS` | Options were supplied that are incompatible with each other
89 | `UNSUPPORTED_PROFILE` | The requested profile is unsupported (`sop generate-key`, `sop encrypt`), or the indicated subcommand does not accept profiles (`sop list-profiles`)
97 | `NO_HARDWARE_KEY_FOUND` | The `sop` implementation  supports some form of hardware-backed secret keys, but could not identify the hardware device (see {{hardware-backed-secrets}})
101 | `HARDWARE_KEY_FAILURE` | The `sop` implementation tried to use a hardware-backed secret key, but the cryptographic hardware refused the operation for some reason other than a bad PIN or password (see {{hardware-backed-secrets}})
103 | `PRIMARY_KEY_BAD` | The primary key of a `KEYS` object is too weak or revoked
107 | `CERT_USERID_NO_MATCH` | The `CERTS` object has no matching User ID
109 | `KEY_CANNOT_CERTIFY` | Key not certification-capable (e.g., expired, revoked, unacceptable usage flags) (`sop certify-userid`)

If a `sop` implementation fails in some way not contemplated by this document, it MAY return `UNSPECIFIED_FAILURE` or any non-zero error code, not only those listed above.

# Known Implementations

The following implementations are known at the time of this draft:

{: title="Known implementations"}
Project name | cli name | notes
---|---|---|---
dkg-sop | `dkg-sop` | Implemented in C++ using the LibTMCG library ({{DKG-SOP}})
gosop | `gosop` | Implemented in golang (Go) using GOpenPGP ({{GOSOP}})
gpgme-sop | `gpgme-sop` | A Rust wrapper around the gpgme C library ({{GPGME-SOP}})
PGPainless SOP | `pgpainless-cli` | Implemented in Java using PGPainless ({{PGPAINLESS-CLI}})
RNP-sop | `rnp-sop` | A Rust wrapper around the librnp C library ({{RNP-SOP}})
rsop | `rsop` | Implemented in Rust using the `rpgpie` crate ({{RSOP}})
Sequoia SOP | `sqop` | Implemented in Rust using the `sequoia-openpgp` crate ({{SQOP}})
sop-openpgp.js | `sop-openpgp.js` | Implemented in JavaScript using OpenPGP.js ({{SOP-OPENPGPJS}})
sopgpy | `sopgpy` | Implemented in Python using PGPy ({{SOPGPY}})

# Alternate Interfaces

This draft primarily defines a command line interface, but future versions may try to outline a comparable idiomatic interface for C or some other widely-used programming language.

Comparable idiomatic interfaces are already active in the wild for different programming languages, in particular:

- Rust: {{RUST-SOP}}
- Java: {{SOP-JAVA}}
- Python: {{PYTHON-SOP}}

These programmatic interfaces are typically coupled with a wrapper that can automatically generate a command-line tool compatible with this draft.

An implementation that uses one of these languages should target the corresponding idiomatic interface for ease of development and interoperability.

# Guidance for Implementers

`sop` uses a few assumptions that implementers might want to consider.

## One OpenPGP Message at a Time

`sop` is intended to be a simple tool that operates on one OpenPGP object at a time.  It should be composable, if you want to use it to deal with multiple OpenPGP objects.

FIXME: discuss what this means for streaming.
The stdio interface doesn't necessarily imply streamed output.

## Simplified Subset of OpenPGP Message

While the formal grammar for OpenPGP Message is arbitrarily nestable, `sop` constrains itself to what it sees as a single "layer" (see {{ciphertext}}).

This is a deliberate choice, because it is what most consumers expect.
Also, if an arbitrarily-nested structure is parsed with a recursive algorithm, this risks a denial of service vulnerability.
`sop` intends to be implementable with a parser that defensively declines to do recursive descent into an OpenPGP Message.

Note that an implementation of `sop decrypt` MAY choose to handle more complex structures, but if it does, it should document the other structures it handles and why it chooses to do so.
We can use such documentation to improve future versions of this spec.

## Validate Signatures Only from Known Signers

There are generally only a few signers who are relevant for a given OpenPGP message.
When verifying signatures, `sop` expects that the caller can identify those relevant signers ahead of time.

## OpenPGP Inputs can be either Binary or ASCII-armored {#optional-input-armoring}

OpenPGP material on input can be in either ASCII-armored or binary form.
This is a deliberate choice because there are typical scenarios where the program can't predict which form will appear.
Expecting the caller of `sop` to detect the form and adjust accordingly seems both redundant and error-prone.

The simple way to detect possible ASCII-armoring is to see whether the high bit of the first octet is set:
{{Section 4.2 of RFC9580}} indicates that bit 7 is always one in the first octet of an OpenPGP packet.
In standard ASCII-armor, the first character is <u>-</u>, so the high bit should be cleared.

When considering an input as ASCII-armored OpenPGP material, `sop` MAY reject an input based on any of the following variations (see {{Section 6.2 of RFC9580}} for precise definitions):

- An unknown Armor Header Line
- Any text before the Armor Header Line
- Malformed lines in the Armor Headers section
- Any non-whitespace data after the Armor Tail
- Any Radix-64 encoded line with more than 76 characters
- Invalid characters in the Radix-64-encoded data
- An invalid Armor Checksum
- A mismatch between the Armor Header Line and the Armor Tail
- More than one ASCII-armored object in the input

For robustness, `sop` SHOULD be willing to ignore whitespace after the Armor Tail.

For any plural data type (i.e.,`SIGNATURES`, `CERTS`, or `KEYS`), the unarmored form is trivially concatenatable with another object of the same type (e.g., with Unix's `cat` utility).
But the armored forms are not concatenatable without first dearmoring.
To avoid inconsistent behavior, a `sop` implementation SHOULD reject anything that appears to be a concatenated series of ASCII-armored objects.

When considering OpenPGP material as input, regardless of whether it is ASCII-armored or binary, `sop` SHOULD reject any material that doesn't produce a valid stream of OpenPGP packets.
For example, `sop` SHOULD raise an error if an OpenPGP packet header is malformed, or if there is trailing garbage after the end of a packet.

For a given type of OpenPGP input material (i.e.,  `SIGNATURES`, `CERTS`, `KEYS`, `INLINESIGNED`, or `CIPHERTEXT`), `sop` SHOULD also reject any input that does not conform to the expected packet stream.
See {{indirect-types}} for the expected packet stream for different types.

## Complexities of the Cleartext Signature Framework {#csf-risks}

`sop` prefers a detached signature as the baseline form of OpenPGP signature, but provides affordances for dealing with inline-signed messages (see `INLINESIGNED`, {{inlinesigned}}) as well.

The most complex form of inline-signed messages is the Cleartext Signature Framework (CSF).
Handling the CSF structure requires parsing to delimit the multiple parts of the document, including at least:

- any preamble before the message
- the inline message header (delimiter line, OpenPGP headers)
- the message itself
- the divider between the message and the signature (including any OpenPGP headers there)
- the signature
- the divider that terminates the signature
- any suffix after the signature

Note also that the preamble or the suffix might be arbitrary text, and might themselves contain OpenPGP messages (whether signatures or otherwise).

If the parser that does this split differs in any way from the parser that does the verification, or parts of the message are confused,
it would be possible to produce a verification status and an actual signed message that don't correspond to one another.

Blurred boundary problems like this can produce ugly attacks similar to those found in [EFAIL].

A user of `sop` that receives an inline-signed message (whether the message uses the CSF or not) can detach the signature from the message with `sop inline-detach` (see {{inline-detach}}).

Alternately, the user can send the message through `sop inline-verify` to confirm required signatures, and then (if signatures are valid) supply its output to the consumer of the signed message.

## Reliance on Supplied Certs and Keys {#cert-validity-performance}

A truly stateless implementation may find that it spends more time validating the internal consistency of certificates and keys than it does on the actual object security operations.

For performance reasons, an implementation may choose to ignore validation on certificate and key material supplied to it.  The security implications of doing so depend on how the certs and keys are managed outside of `sop`.

## Text is always UTF-8 {#utf8}

Various places in this specification require UTF-8 {{!RFC3629}} when encoding text. `sop` implementations SHOULD NOT consider textual data in any other character encoding.

OpenPGP Implementations MUST already handle UTF-8, because various parts of {{RFC9580}} require it, including:

- User ID
- Notation name
- Reason for revocation
- ASCII-armor Comment: header

Dealing with messages in other charsets leads to weird security failures like {{Charset-Switching}}, especially when the charset indication is not covered by any sort of cryptographic integrity check.
Restricting textual data to `UTF-8` universally across the OpenPGP ecosystem eliminates any such risk without losing functionality, since `UTF-8` can encode all known characters.

## Passwords are Human-Readable {#human-readable-passwords}

Passwords are generally expected to be human-readable, as they are typically recorded and transmitted as human-visible, human-transferable strings.
However, they are used in the OpenPGP protocol as bytestrings, so it is important to ensure that there is a reliable bidirectional mapping between strings and bytes.
The maximally robust behavior here is for `sop encrypt` and `sop generate-key` (that is, commands that use a password to encrypt) to constrain the choice of passwords to strings that have such a mapping,
and for `sop decrypt` and `sop sign` (and `sop inline-sign`, as well as`sop encrypt` when decrypting a signing key; that is, commands that use a password to decrypt) to try multiple plausible versions of any password supplied by `PASSWORD`.

### Generating Material with Human-Readable Passwords {#generating-human-readable}

When generating material based on a password, `sop encrypt` and `sop generate-key` enforce that the password is actually meaningfully human-transferable.
In particular, an implementation generating material based on a new password SHOULD apply the following considerations to the supplied password:

- require `UTF-8`
- trim trailing whitespace

Some `sop encrypt` and `sop generate-key` implementations may make even more strict requirements on input to ensure that they are transferable between humans in a robust way.

For example, a more strict `sop encrypt` or `sop generate-key` MAY also:

- forbid leading whitespace
- forbid non-printing characters other than `SPACE (U+0020)`, such as `ZERO WIDTH NON-JOINER (U+200C)` or `TAB (U+0009)`
- require the password to be in Unicode Normal Form C ({{UNICODE-NORMALIZATION}})

Violations of these more-strict policies SHOULD result in an error of `PASSWORD_NOT_HUMAN_READABLE`.

A `sop encrypt` or `sop generate-key` implementation typically SHOULD NOT attempt enforce a minimum "password strength",
but in the event that some implementation does, it MUST NOT represent a weak password with `PASSWORD_NOT_HUMAN_READABLE`.

### Consuming Password-protected Material {#consuming-passwords}

When `sop decrypt` receives a `PASSWORD` input, either from a `--with-key-password` or `--with-password` option, it sees its content as a bytestring.
`sop sign` also sees the content of any `PASSWORD` input supplied to its `--with-key-password`  option as a bytestring.
If the bytestring fails to work as a password, but ends in `UTF-8` whitespace, it will try again with the trailing whitespace removed.
This handles a common pattern of using a file with a final newline, for example.
The pattern here is one of robustness in the face of typical errors in human-transferred textual data.

A more robust `sop decrypt` or `sop sign` implementation that finds neither of the above two attempts work for a given `PASSWORD` MAY try additional variations if they produce a different bytestring, such as:

- trimming any leading whitespace, if discovered
- trimming any internal non-printable characters other than `SPACE (U+0020)`
- converting the supplied `PASSWORD` into Unicode Normal Form C ({{UNICODE-NORMALIZATION}})

A `sop decrypt` or `sop sign` implementation that stages multiple decryption attempts like this SHOULD consider the computational resources consumed by each attempt, to avoid presenting an attack surface for resource exhaustion in the face of a non-standard `PASSWORD` input.

## Be Careful with Special Designators {#special-designators-guidance}

As documented in {{special-designators}}, special designators for indirect inputs like `@ENV:` and `@FD:` (and indirect outputs using `@FD:`) warrant some special/cautious handling.

For one thing, it's conceivable that the filesystem could contain a file with these literal names.
If `sop` receives an indirect output parameter that starts with an <u>@</u> it MUST NOT write to the filesystem for that parameter.
A `sop` implementation that receives such a parameter as input MAY test for the presence of such a file in the filesystem and fail with `AMBIGUOUS_INPUT` to warn the user of the ambiguity and possible confusion.

These special designators are likely to be used to pass sensitive data (like secret key material or passwords) so that it doesn't need to touch the filesystem.
Given this sensitivity, `sop` should be careful with such an input, and minimize its leakage to other processes.
In particular, `sop` SHOULD NOT leak any environment variable identified by `@ENV:` or file descriptor identified by `@FD:` to any subprocess unless the subprocess specifically needs access to that data.

## Nuances for Hardware-backed Secret Key Material {#hardware-backed-secrets}

There are a number of limitations and nuances to be aware of for hardware-backed secret key support in this interface.
Some `sop` implementations will simply not support hardware-backed secret key material.
Other implementations might support only a single kind of hardware-backing (e.g., an OpenPGP Smartcard {{OPENPGP-SMARTCARD}} but not a TPM, or vice versa).

There is no formally adopted OpenPGP standard for identifying that a given secret key is backed by hardware based on the OpenPGP wire format.
{{?I-D.dkg-openpgp-hardware-secrets}} proposes one simple and straightforward approach for how the wire format could cover this use case.
This simple mechanism is deliberately agnostic about the specific kind of cryptographic hardware, but it does imply a sort of rough shape of what the interface to the hardware would permit.
In particular, it will work best with hardware that has the following properties:

- The hardware does specific asymmetric secret key operations, using secret keys that it does not release.

- The user can ask the hardware to provide a list of corresponding public key material (or OpenPGP key fingerprints) for any of the secret keys held by the device.

- The hardware MAY require the provision of a PIN or password to enable secret key operation, but does not require a PIN or password for the list of public key material.

The `sop` interface does not currently provide for provisioning cryptographic hardware with secret key material, or for changing the PIN or password for the cryptographic hardware.
Users of cryptographic hardware need to do provisioning and PIN or password setting outside of `sop`.

If a user has two attached hardware tokens that both hold the same secret key, and they are both password-locked, and they use different passwords, `sop` offers no way for the user to clearly indicate which password belongs to which device.
Some cryptographic hardware is designed to lock the device if the wrong password is entered too many times, so users in this configuration are at risk of accidental lockout.
The easiest resolution for this is for the user to detach any duplicate devices before invoking `sop`.

Note that some OpenPGP implementations use the private codepoint ranges in the OpenPGP specification within an OpenPGP Transferable Secret Key (e.g., {{GNUPG-SECRET-STUB}}) to indicate that the secret key can be found on a smartcard.

While hardware-backed secret key operations can be significantly slower than modern computers, and physical affordances like button-presses or NFC tapping can themselves incur delay, it's bad form for an invocation of `sop` to hang forever.
This specification doesn't define a specific maximum allowable delay, but if an implementation calls into a hardware device either for public key listing or for secret key operations, it should not allow the cryptographic hardware to take an arbitrary amount of time to respond.

# Guidance for Consumers

While `sop` is originally conceived of as an interface for interoperability testing, it's conceivable that an application that uses OpenPGP for object security would want to use it.

FIXME: more guidance for how to use such a tool safely and efficiently goes here.

FIXME: if an encrypted OpenPGP message arrives without metadata, it is difficult to know which signers to consider when decrypting.
How do we do this efficiently without invoking `sop decrypt` twice, once without `--verify-*` and again with the expected identity material?

## Choosing Between --as=text and --as=binary

A program that invokes `sop` to generate an OpenPGP signature typically needs to decide whether it is making a text or binary signature.

By default, `sop` will make a binary signature.
The caller of `sop sign` should choose `--as=text` only when it knows that:

- the data being signed is in fact textual, and encoded in `UTF-8`, and
- the signed data might be transmitted to the recipient (the verifier of the signature) over a channel that has the propensity to transform line-endings.

Examples of such channels include FTP ({{?RFC0959}}) and SMTP ({{?RFC5321}}).

## Special Designators and Unusual Filenames

In some cases, a user of `sop` might want to pass all the files in a given directory as positional parameters (e.g., a list of CERTS files to test a signature against).

If one of the files has a name that starts with `--`, it might be confused by `sop` for an option.
If one of the files has a name that starts with `@`, it might be confused by `sop` as a special designator ({{special-designators}}).

If the user wants to deliberately refer to such an ambiguously-named file in the filesystem, they should prefix the filename with  `./` or use an absolute path.

Any specific `@FD:` special designator SHOULD NOT be supplied more than once to an invocation of `sop`.
If a `sop` invocation sees multiple copies of a specific `@FD:n` input (e.g., `sop sign @FD:3 @FD:3`),
it MAY fail with `MISSING_INPUT` even if file descriptor 3 contains a valid `KEYS`, because the bytestream for the `KEYS` was consumed by the first argument.
Doubling up on the same `@FD:` for output (e.g., `sop decrypt --session-key-out=@FD:3 --verifications-out=@FD:3`) also results in an ambiguous data stream.

# Security Considerations

The OpenPGP object security model is typically used for confidentiality and authenticity purposes.

## Signature Verification {#signature-verification}

In many contexts, an OpenPGP signature is verified to prove the origin and integrity of an underlying object.

When `sop` checks a signature over data (e.g., via `sop verify` or `sop decrypt --verify-with`), it MUST NOT consider it to be verified unless all of these conditions are met:

- The signature must be made by a signing-capable public key that is present in one of the supplied certificates
- The certificate and signing subkey must have been created before or at the signature time
- The certificate and signing subkey must not have been expired at the signature time
- The certificate and signing subkey must not be revoked with a "hard" revocation
- If the certificate or signing subkey is revoked with a "soft" revocation, then the signature time must predate the revocation
- The signing subkey must be properly bound to the primary key, and cross-signed
- The signature (and any dependent signature, such as the cross-sig or subkey binding signatures) must be made with strong cryptographic algorithms (e.g., not `MD5` or a 1024-bit `RSA` key)
- The signature must be of type 0x00 ("Signature of a binary document") or 0x01 ("Signature of a canonical text document"); other signature types are inappropriate for data signatures

Implementers MAY also consider other factors in addition to the origin and authenticity, including application-specific information.

For example, consider the application domain of checking software updates.
If software package Foo version 13.3.2 was signed on 2019-10-04, and the user receives a copy of Foo version 12.4.8 that was signed on 2019-10-16, it may be authentic and have a more recent signature date.
But it is not an upgrade (12.4.8 < 13.3.2), and therefore it should not be applied automatically.

In such cases, it is critical that the application confirms that the other information verified is *also* protected by the relevant OpenPGP signature.

Signature validity is a complex topic (see for example the discussion at {{DISPLAYING-SIGNATURES}}), and this documentation cannot list all possible details.

### Explaining Non-Verification on Standard Error {#explaining-non-verification}

When verifying OpenPGP signatures, sometimes no valid signatures are found.
This will cause the verifying subcommand to produce an empty `VERIFICATIONS` output, and for some subcommands (`sop verify` and `sop inline-verify` in particular) will also cause the subcommand to fail with `NO_SIGNATURE`.

When this happens, some consumers will want to know more details about the verification failure, since some verification failures may be indications that something is wrong with the verifier's setup, such as outdated OpenPGP implementations (which can be upgraded), expired signing certificates (which can be refreshed), and so on.

To address this, when no valid signatures are found at all, `sop` MAY emit a human-readable explanation to standard error.

Some example explanations for complete signature validation failure include:

- Version 7 signature found, but FooPGP 2.0.3 only supports versions 4 and 6.
- Version 3 signature found, but BarPGP 0.9.7 rejects all version 3 signatures.
- Signature from pubkey algorithm 94 found, but BazPGP 1.1 does not support this pubkey algorithm.
- Signature using hash algorithm 22 found, but QuxPGP 19.0.5 does not support this hash algorithm.
- Two signatures found, both made by unknown OpenPGP certificates.
- Signature does not match hash prefix.
- No OpenPGP signatures found.

In some cases (such as when two OpenPGP signatures are discovered, and they both fail to validate for different reasons), a `sop` implementation may choose to emit a more complex warning.

Unless `--debug` is present, `sop` SHOULD NOT emit any such warning (even if true for one of the OpenPGP signatures found) if another signature was found in the same `SIGNATURES` object or `INLINESIGNED` message that does verify correctly.
This keeps the upgrade path smooth for the whole ecosystem.
As the ecosystem evolves, signatures using new versions and algorithms, or signatures simply using new signing keys, are typically introduced as a second signature distributed alongside the first.
A warning about a signature with a new or unknown algorithm (or key) when an accompanying signature still verifies from a known key with a known algorithm will discourage signers from adopting new algorithms or keys.
And introducing a warning about a signature using a deprecated algorithm (or key), when an accompanying signature still verifies using a more modern algorithm or key will discourage a verifier from upgrading their OpenPGP implementation or dropping old, deprecated keys.

Implementers should avoid emitting dangerous explanations.
For example, an explanation like "Signature from 0xDEADBEEF found, but not in list of acceptable signers" might encourage a user to go hunting for any certificate with short key ID 0xDEADBEEF and start using it to verify signatures.
This would be a very dangerous explanation, in particular because short key IDs are trivially forgeable.
But it would also be nearly as dangerous to use a full fingerprint (instead of a short Key ID) in such a message because then all an attacker has to do is to get their signature to appear in the place where the verifier is looking for a signature, and then the warning will encourage the verifier go look up the attacker's certificate by fingerprint.

An internationalized, locale-aware `sop` implementation should localize these warning messages.

## Compression {#compression}

The interface as currently specified does not allow for control of compression.
Compressing and encrypting data that may contain both attacker-supplied material and sensitive material could leak information about the sensitive material (see the CRIME attack).

Unless an application knows for sure that no attacker-supplied material is present in the input, it should not compress during encryption.

# Privacy Considerations

Material produced by `sop encrypt` may be placed on an untrusted machine (e.g., sent through the public `SMTP` network).
That material may contain metadata that leaks associational information (e.g., recipient identifiers in PKESK packets ({{Section 5.1 of RFC9580}})).
FIXME: document things like PURBs and `--hidden-recipient`)

## Object Security vs. Transport Security

OpenPGP offers an object security model, but says little to nothing about how the secured objects get to the relevant parties.

When sending or receiving OpenPGP material, the implementer should consider what privacy leakage is implicit with the transport.

--- back

# sopv Version Changelog {#sopv-changelog}

This is a reverse-chronological order changelog for the `sopv` subset.

## sopv Version 1.0 {#sopv-1.0}

The following subcommands:

- `sop version`
- `sop verify`
- `sop inline-verify`

And the following features:

- Special designators `@FD:` and `@ENV:` as input for `CERTS` objects
- Special designator `@FD:` as possible output for `--verifications-out` argument to `sopv inline-verify`
- Multiple certificates in each `CERTS` object
- `--not-before` and `--not-after` constraints

# C Library API (Tentative) {#libsop}

As specified in this draft, SOP is a command-line tool.

However, it can also be useful to have a comparable API exposed as a C library.
This library can be implemented as a shared object (e.g., `.so`, `.dll`, or `.dylib` depending on the platform) or as a statically linked object.
This interface can be reused in many different places, as most modern programming languages offer "bindings" to C libraries.

A proposed interface to a C library follows here as a C header file.

The primary goal of this shared object interface is to make it easy to implement the command-line interface described in this document.
That said, it is also intended to be relatively ergonomic to use in plausible OpenPGP workflows where the caller has access to all of the explicit state.

If there is a plausible OpenPGP workflow that is not supported by this library API, please propose improvements and explain the specific workflow.

{: sourcecode-name="sop.h"}
~~~ text/x-chdr
{::include sop.h}
~~~

This proposed interface currently deals only with signing.
Encryption and decryption will be added in a future revision.

## Design Choices for Library API

The library is deliberately minimal, with data types and functionality corresponding to the SOP CLI.
The interface itself should expose no dependencies beyond libc.

All datatypes are opaque structs.
Library implementations MUST NOT expose library users to the memory layout of the underlying objects.

The library deals with data that is all in RAM, and produces data in RAM.
For simplicity, it does not currently expose a streaming interface.

It should be fairly straightforward to implement the SOP CLI on top of such a library.

## Library Use Patterns

There are two main kinds of data structures: operations (e.g., `sop_op_sign` and `sop_op_verify`) and datatypes (e.g., `sop_keys` and `sop_certs`).

Operation objects are one-shot objects.
They are used in the following pattern:

- create an operations object (`sop_op_*_new`)
- adjust it to behave in certain ways (e.g., `sop_op_sign_use_keys`, `sop_op_verify_not_before`)
- execute it (with some specific `sop_op_*_execute` function)
- dispose of it (`sop_op_*_free`)

The library user MUST NOT execute the same operation object more than once.
When a single operation object is executed more than once, it should fail with `SOP_OPERATION_ALREADY_EXECUTED`.
FIXME: if a use case arises with a reasonable need to re-execute an already adjusted object, we could extend the API to allow the user to clone an object.

Datatype objects are reusable objects.
For example, it is fine for a library user to pass the same `sop_certs` to multiple `sop_op_*` operation objects, as long as the `sop_certs` object is not freed before the execution of all the operation objects it has been passed to.

Datatype objects are also immutable.
Any function which modifies a datatype object always creates a new copy of the object, with the specific change applied.
This immutability avoids any ambiguity about what should happen when a datatype object is adjusted after it was passed to an operation object but before it was executed.

## `libsopv` C API Subset

A minimalist library subset that only does OpenPGP signature verification might be called `libsopv`.
This library is useful wherever the use case is just OpenPGP signature verification.

Such a library MUST implement the following functions from the C API:

- `sop_ctx_new`
- `sop_ctx_free`
- `sop_set_log_function`
- `sop_set_log_level`
- `sop_version`
- `sop_version_backend`
- `sop_version_extended`
- `sop_buf_size`
- `sop_buf_data`
- `sop_buf_free`
- `sop_certs_from_bytes`
- `sop_certs_free`
- `sop_sigs_from_bytes`
- `sop_sigs_free`
- `sop_verifications_count`
- `sop_verifications_get_time`
- `sop_verifications_get_mode`
- `sop_verifications_to_text`
- `sop_verifications_free`
- `sop_inlinesigned_from_bytes`
- `sop_op_verify_new`
- `sop_op_verify_not_before`
- `sop_op_verify_not_after`
- `sop_op_verify_add_signers`
- `sop_op_verify_detached_execute`
- `sop_op_verify_inline_execute`
- `sop_op_verify_free`

This minimal library interface should be sufficient to implement the `sopv` subset version 1.0 (see {{sopv}}).

# Simple CLI Test {#simple-self-test}

The following POSIX-compliant shell script can be pointed to a SOP implementation.
It will report which subcommands have basic coverage.

It does not consider all possible combinations of all options.

{: sourcecode-name="simple-sop-test"}
~~~ text/x-sh
{::include test/simple-sop-test}
~~~

# Testing the `sopv` Subset  {#sopv-subset-test}

The following two POSIX-compliant shell scripts can be used (with a signing-capable `sop` implementation) to exhaustively test a `sopv` implementation.

First, use `setup-sopv-test` with a signing-capable `sop` implementation to generate a set of test vectors in the current working directory.
Then, run `sopv-test` against the `sopv` implementation:

~~~
./setup-sopv-test some-sop
./sopv-test my-sopv
~~~

## `setup-sopv-test`

{: sourcecode-name="setup-sopv-test"}
~~~ text/x-sh
{::include test/setup-sopv-test}
~~~

## `sopv-test`

{: sourcecode-name="sopv-test"}
~~~ text/x-sh
{::include test/sopv-test}
~~~

# Acknowledgements

This work was inspired by Justus Winter's {{OpenPGP-Interoperability-Test-Suite}}.

The following people contributed helpful feedback and considerations to this draft, but are not responsible for its problems:

- Allan Nordhøy
- Antoine Beaupré
- Edwin Taylor
- Guillem Jover
- Heiko Schaefer
- Jameson Rollins
- Justus Winter
- Paul Schaub
- Vincent Breitmoser

# Future Work

- certificate transformation into popular publication forms:
   - WKD
   - DANE OPENPGPKEY
   - Autocrypt
- `sop encrypt` -- specify compression? (see {{compression}})
- `sop encrypt` -- specify padding policy/mechanism?
- `sop decrypt` -- how can it more safely handle zip bombs?
- `sop decrypt` -- what should it do when encountering weakly-encrypted (or unencrypted) input?
- `sop encrypt` -- minimize metadata (e.g., `--throw-keyids`)?
- specify an error if a `DATE` arrives as input without a time zone?
- add considerations about what it means for armored `CERTS` to contain multiple certificates -- multiple armorings?  one big blob?
- do we need an interface or option (for performance?) with the semantics that `sop` doesn't validate certificates internally, it just accepts whatever's given as legit data? (see {{cert-validity-performance}})
- do we need to be able to convert a message with a text-based signature to a CSF `INLINESIGNED` message? I'd rather not, given the additional complications.
- add encryption and decryption to C Library API

# Document History

## Substantive Changes between -10 and -11:

- `update-key`: new key management subcommand
- `merge-certs`: new certificate management subcommand
- `certify-userid`: new User ID certification subcommand
- `validate-userid`: new User ID verification subcommand
- Replace references to RFC 4880 with RFC 9580
- Set aside error 1 as `UNSPECIFIED_FAILURE`
- Encourage JSON output in tail of `VERIFICATIONS` lines
- Add universal (ignorable) `--debug` option
- Add simple (and incomplete) shell-script test in appendix

## Substantive Changes between -09 and -10:

- drop `@HARDWARE:` special designator in favor of the simple {{I-D.dkg-openpgp-hardware-secrets}} or other magic
- drop hardware-specific C API function
- define SemVer-versioned `sopv` subset of CLI
- `sop version`: add `--sopv` option
- define libsopv subset of C API
- explicitly require `BAD_DATA` failure when `KEYS` are passed as `CERTS`

## Substantive Changes between -08 and -09:

- enable the use of hardware-backed secret key material via the `@HARDWARE:` special designator
- C API: clarify design goals and usage patterns
- C API: major overhaul and normalization:
  - allow passthrough "cookie" for logging
  - allow NULL return from `sop_version_*`
  - explicitly offer `SOP_LOG_NEVER`
  - use `*_from_bytes` and `*_to_bytes` instead of `*_import` and `*_export`
  - datatype objects are now immutable
  - operation objects are one-shot
  - always return `sop_err`, even at a slight cost to C caller ergonomics

## Substantive Changes between -07 and -08:

- `revoke-key`, `change-key-password`: add `--no-armor` option
- `generate-key`: should fail on non-UTF-8 `USERID`
- `generate-key`: acknowledge that implementations MAY reject `USERID`s that seem bad
- `armor`: drop `--label` option
- `encrypt`: add `--session-key-out` option
- ASCII-armored objects should not be concatenated
- signature verification should only work for sigtypes 0x00 (binary) and 0x01 (canonical text)
- `sign`: Constrain input when `--micalg-out` is present for alignment with {{RFC3156}}
- propose simple C API for signing and verification

## Substantive Changes between -06 and -07:

- `generate-key`: add `--signing-only` option
- new key management subcommand: `change-key-password`
- new key management subcommand: `revoke-key`

## Substantive Changes between -05 and -06:

- `version`: add `--sop-spec` argument
- `encrypt`: add `--profile` argument

## Substantive Changes between -04 and -05:

- `decrypt`: change `--verify-out` to `--verifications-out`
- `encrypt`: add missing `--with-key-password`
- add the concept of "profiles", use with `generate-key`
- include table of known implementations
- `VERIFICATIONS` can now indicate the type of the signature (`mode:text` or `mode:binary`)

## Substantive Changes between -03 and -04:

- Reinforce that PASSWORD and SESSIONKEY are indirect data types
- `encrypt`: remove `--as=mime` option
- Handle password-locked secret key material: add `--with-key-password` options to `generate-key`, `sign`, and `decrypt`.
- Introduce `INLINESIGNED` message type ({{inlinesigned}})
- Rename `detach-inband-signature-and-message` to `inline-detach`, clarify its possible inputs
- Add `inline-verify`
- Add `inline-sign`

## Substantive Changes between -02 and -03:

- Added `--micalg-out` parameter to `sign`
- Change from `KEY` to `KEYS` (permit multiple secret keys in each blob)
- New error code: `KEY_CANNOT_SIGN`
- `version` now has `--backend` and `--extended` options

## Substantive Changes between -01 and -02:

- Added mnemonics for return codes
- `decrypt` should fail when asked to output to a pre-existing file
- Removed superfluous `--armor` option
- Much more specific about what `armor --label=auto` should do
- `armor` and `dearmor` are now fully idempotent, but work only well-formed OpenPGP streams
- Dropped `armor --allow-nested`
- Specified what `encrypt --as=` means
- New error code: `KEY_IS_PROTECTED`
- Documented expectations around human-readable, human-transferable passwords
- New subcommand: `detach-inband-signature-and-message`
- More specific guidance about special designators like `@FD:` and `@ENV:`, including new error codes `UNSUPPORTED_SPECIAL_PREFIX` and `AMBIGUOUS_INPUT`

## Substantive Changes between -00 and -01:

- Changed `generate` subcommand to `generate-key`
- Changed `convert` subcommand to `extract-cert`
- Added "Input String Types" section as distinct from indirect I/O
- Made implicit arguments potentially explicit (e.g., `sop armor --label=auto`)
- Added `--allow-nested` to `sop armor` to make it idempotent by default
- Added fingerprint of signing (sub)key to `VERIFICATIONS` output
- Dropped `--mode` and `--session-key` arguments for `sop encrypt` (no plausible use, not needed for interop)
- Added `--with-session-key` argument to `sop decrypt` to allow for session-key-based decryption
- Added examples to each subcommand
- More detailed error codes for `sop encrypt`
- Move from `CERT` to `CERTS` (each `CERTS` argument might contain multiple certificates)

